The OpenShift `jbossews` cartridge documentation can be found at:

http://openshift.github.io/documentation/oo_cartridge_guide.html#tomcat

To Test local :
add the following to your system environment variables:

OPENSHIFT_MONGODB_DB_HOST="localhost" 
OPENSHIFT_MONGODB_DB_PORT="42071" 
OPENSHIFT_APP_NAME="appdatez"
OPENSHIFT_MONGODB_DB_USERNAME="admin" 
OPENSHIFT_MONGODB_DB_PASSWORD="bDVEL97nFiAA"	

and run the following command:
rhc port-forward -a appdatez
---
disk usage
rhc app show appdatez --gears quota

see the most heavy folders
rhc ssh appdatez
du -h * | sort -rh | head -50

clean up logs
rhc app-tidy appdatez