package org.chats;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.method.HandlerMethod;

@Ignore
@WebAppConfiguration
public class AbstractRestInit extends AbstractBusinessInit {

	@Autowired
	private WebApplicationContext wac;
	
	@Resource
    private FilterChainProxy springSecurityFilterChain;

	private MockMvc mockMvc;
	
	private String loggedInUser;

	@Before
	public void setup() {
		this.mockMvc = webAppContextSetup(this.wac).addFilter(springSecurityFilterChain).build();
		loggedInUser = ADMIN_PHONE;
	}
	
	protected MvcResult getCall(String url, boolean authorized, HttpStatus status, boolean validateHandler) throws Exception{
		MvcResult result = this.mockMvc.perform(get(url).secure(true)
				.session(login(authorized))
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().is(status.value())).andReturn();
		
		if (validateHandler){
			return validateHandler(result);
		}
		return result;
	}

	private String getLoggedUser() {
		return loggedInUser;
	}
	
	protected MvcResult postCall(String url, String payload, boolean authorized, HttpStatus status, boolean validateHandler) throws Exception{
		MvcResult result = this.mockMvc.perform(post(url).contentType(MediaType.APPLICATION_JSON).content(payload).secure(true)
				.session(login(authorized))
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().is(status.value())).andReturn();
		
		if (validateHandler){
			return validateHandler(result);
		}
		return result;
	}
	
	protected MvcResult putCall(String url, String payload, boolean authorized, HttpStatus status, boolean validateHandler) throws Exception{
		MvcResult result = this.mockMvc.perform(put(url).contentType(MediaType.APPLICATION_JSON).content(payload).secure(true)
				.session(login(authorized))
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().is(status.value())).andReturn();
		
		if (validateHandler){
			return validateHandler(result);
		}
		return result;
	}
	
	protected MvcResult deleteCall(String url, boolean authorized, HttpStatus status, boolean validateHandler) throws Exception{
		MvcResult result = this.mockMvc.perform(delete(url).secure(true)
				.session(login(authorized))
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().is(status.value())).andReturn();
		
		if (validateHandler){
			return validateHandler(result);
		}
		return result;
	}
	
	private MvcResult validateHandler(MvcResult result){
		String handlerName = ((HandlerMethod)result.getHandler()).getBean().getClass().getName();
		Assert.assertTrue("Wrong handler: " + handlerName
				, String.format(handlerName + "%s", "Test").equals(this.getClass().getName()));
		return result;
	}
	
	protected <T extends Object> T resolveResult(MvcResult mvcResult, String property, Class<T> clazz){
		Object value = mvcResult.getModelAndView().getModel().get(property);
		Assert.assertNotNull("Response value is missing", value);
		return clazz.cast(value);
	}
	
	public void setLogInUser(String userId){
		loggedInUser = userId;
	}
	
	private MockHttpSession login(boolean authorized) throws Exception{
		if (authorized) {
			String loggedUser = getLoggedUser();
			MvcResult andReturn = this.mockMvc.perform(post("/j_spring_security_check")
					.contentType(MediaType.APPLICATION_FORM_URLENCODED)
					.param("j_username", loggedUser)
					.param("j_password", getUserPassword(loggedUser))
					.param("submit", "Login")
					.secure(true))
					.andReturn();
			return (MockHttpSession)andReturn.getRequest().getSession();
		}
		return new MockHttpSession();
	}
}
