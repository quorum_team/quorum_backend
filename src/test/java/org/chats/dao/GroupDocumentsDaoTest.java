package org.chats.dao;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.chats.AbstractDaoInit;
import org.chats.authorization.ContextHolder;
import org.chats.db.dto.GroupDocumentDto;
import org.chats.db.dto.TaskDto;
import org.chats.enums.Roles;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class GroupDocumentsDaoTest extends AbstractDaoInit {
	
	@Mock
	private ContextHolder contextHolder;
	
	private long currentTime;

	@Test
	public void crud(){
		GroupDocumentDto groupDocument = new GroupDocumentDto();
		String id = groupDocument.getId();
		groupDocumentsDao.create(groupDocument);
		groupDocument = groupDocumentsDao.getById(id);
		String createdBy = groupDocument.getCreatedBy();
		long createdAt = groupDocument.getCreatedAt();
		assertTrue("Create By Should Not Be Empty", ADMIN_PHONE.equals(createdBy)); 
		assertTrue("Create Time should be bigger then start test time", createdAt >= currentTime);
		groupDocumentsDao.update(groupDocument, ADMIN_PHONE);
		groupDocument = groupDocumentsDao.getById(id);
		assertTrue("Create By Should Not Be Empty", createdBy.equals(groupDocument.getCreatedBy())); 
		assertTrue("Create Time should be bigger then start test time", createdAt == groupDocument.getCreatedAt());
		assertTrue("Update By Should Not Be Empty", ADMIN_PHONE.equals(groupDocument.getUpdatedBy())); 
		assertTrue("Update Time should be bigger then create time", groupDocument.getUpdatedAt() > createdAt);
		groupDocumentsDao.delete(groupDocument);
		groupDocument = groupDocumentsDao.getById(id);
		assertNull("Group Document Should have been deleted", groupDocument);
	}
	
	@Before
	public void init(){
		currentTime = System.currentTimeMillis();
		MockitoAnnotations.initMocks(this);
		when(contextHolder.getLoggedUserPhone()).thenReturn(ADMIN_PHONE);
	}
	
	@Test
	public void addTask(){
		GroupDocumentDto groupDocument = new GroupDocumentDto();
		String id = groupDocument.getId();
		TaskDto task = new TaskDto();
		groupsDao.create(groupDocument);
		groupsDao.pushTask(task, id);
		groupDocument = groupDocumentsDao.getById(id);
		assertTrue(groupDocument.getTasks().get(0).getId().equals(task.getId()));
		groupsDao.pullTask(task.getId());
		groupDocument = groupDocumentsDao.getById(id);
		assertTrue(groupDocument.getTasks().size() == 0);
		groupDocumentsDao.delete(groupDocument);
	}
	
	@Test
	public void roles(){
		GroupDocumentDto groupDocument = new GroupDocumentDto();
		String id = groupDocument.getId();
		groupDocumentsDao.create(groupDocument);
		groupDocumentsDao.assignUserRole(Roles.ADMIN, "123", id);
		groupDocumentsDao.assignUserRole(Roles.AUTHOR, "234", id);
		groupDocumentsDao.assignUserRole(Roles.USER, "345", id);
		groupDocument = groupDocumentsDao.getById(id);
		assertTrue(groupDocument.getRoles().getAdmins().size() > 0);
		assertTrue(groupDocument.getRoles().getAuthors().size() > 0);
		assertTrue(groupDocument.getRoles().getUsers().size() > 0);
		groupDocumentsDao.removeUserRole(id, "123");
		groupDocumentsDao.removeUserRole(id, "234");
		groupDocumentsDao.removeUserRole(id, "345");
		groupDocument = groupDocumentsDao.getById(id);
		assertTrue(groupDocument.getRoles().getAdmins().size() == 0);
		assertTrue(groupDocument.getRoles().getAuthors().size() == 0);
		assertTrue(groupDocument.getRoles().getUsers().size() == 0);
		groupDocumentsDao.delete(groupDocument);
	}
}
