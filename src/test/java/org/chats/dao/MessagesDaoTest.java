package org.chats.dao;

import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.chats.AbstractDaoInit;
import org.chats.db.dto.MessageContentDto;
import org.chats.enums.MessageType;
import org.junit.Test;

public class MessagesDaoTest extends AbstractDaoInit{
	
	@Test
	public void messages(){
		MessageContentDto message = new MessageContentDto();
		message.setContent("a");
		message.setCreateAt(2l);
		message.setCreateBy(ADMIN_PHONE);
		message.setGroupId("123");
		message.setType(MessageType.TEXT);
		Set<String> users = new HashSet<String>();
		users.add(USER_PHONE);
		message.setUsers(users);
		messagesDao.upsert(message);
		List<MessageContentDto> messagesByPhone = messagesDao.getMessagesByPhone(USER_PHONE);
		assertTrue(messagesByPhone.get(0).getId().equals(message.getId()));
		messagesDao.pullUser(message.getId(), USER_PHONE);
		messagesByPhone = messagesDao.getMessagesByPhone(USER_PHONE);
		assertTrue(messagesByPhone.size() == 0);
		messagesDao.removeOrphanMessages();
	}
}
