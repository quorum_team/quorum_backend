package org.chats.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.chats.AbstractDaoInit;
import org.chats.db.dto.UserDto;
import org.chats.enums.UserType;
import org.junit.Test;
import org.springframework.mobile.device.DevicePlatform;

public class UsersDaoTest extends AbstractDaoInit{
	
	@Test
	public void createUser(){
		UserDto user = new UserDto();
		user.setCode("12345");
		user.setDevice(DevicePlatform.IOS);
		user.setName("Test User");
		user.setPhone("123456");
		user.setUserType(UserType.PREMIUM);
		usersDao.addUser(user);
		verifyUser(user, true);
		user.setCode("54321");
		user.setDevice(DevicePlatform.ANDROID);
		user.setName("New Test User");
		user.setPhone("1234567");
		user.setUserType(UserType.REGULAR);
		usersDao.updateUser(user);
		verifyUser(user, true);
		usersDao.removeUser(user.getId());
		verifyUser(user, false);
	}
	
	private void verifyUser(UserDto user, boolean exists){
		if (exists){
			List<UserDto> allUsers = usersDao.getAllUsers();
			verifyUser(user, allUsers);
			allUsers = usersDao.getRegisteredUsers(new ArrayList<String>(Arrays.asList(user.getPhone())));
			verifyUser(user, allUsers);
			verifyUserProps(user, usersDao.getUserById(user.getId()));
			verifyUserProps(user, usersDao.getUserByPhoneAndCode(user.getPhone(), user.getCode()));
			verifyUserProps(user, usersDao.getUserByPhone(user.getPhone()));
		} else {
			List<UserDto> allUsers = usersDao.getAllUsers();
			assertNull(getUser(allUsers, user));
			allUsers = usersDao.getRegisteredUsers(new ArrayList<String>(Arrays.asList(user.getPhone())));
			assertNull(getUser(allUsers, user));
			assertNull(usersDao.getUserById(user.getId()));
			assertNull(usersDao.getUserByPhoneAndCode(user.getPhone(), user.getCode()));
			assertNull(usersDao.getUserByPhone(user.getPhone()));
		}
	}

	private void verifyUser(UserDto user, List<UserDto> allUsers) {
		UserDto actual;
		actual = getUser(allUsers, user);
		assertNotNull("User Not Found", actual);
		verifyUserProps(user, actual);
	}
	
	private UserDto getUser(List<UserDto> users, UserDto expected){
		UserDto result = null;
		for(UserDto user : users){
			if (user.getId().equals(expected.getId())){
				result = user;
				break;
			}
		}
		return result;
	}
	
	private void verifyUserProps(UserDto expected, UserDto actual){
		assertTrue(expected.getCode().equals(actual.getCode()));
		assertTrue(expected.getDevice().equals(actual.getDevice()));
		assertTrue(expected.getName().equals(actual.getName()));
		assertTrue(expected.getPhone().equals(actual.getPhone()));
		assertTrue(expected.getUserType().equals(actual.getUserType()));
	}
}
