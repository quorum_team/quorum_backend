package org.chats.dao;

import static org.junit.Assert.assertTrue;

import org.chats.AbstractDaoInit;
import org.chats.db.dto.RegisterDto;
import org.junit.Test;

public class RegisterDaoTest extends AbstractDaoInit{
	
	@Test
	public void register(){
		String phoneNmber = "123321";
		String ip = "12.34.56.78";
		registerDao.addEntry(ip, phoneNmber, 1l);
		RegisterDto reg = registerDao.getById(phoneNmber);
		assertTrue(reg.getIps().size() == 1);
		assertTrue(reg.getIps().iterator().next().equals(ip));
		assertTrue(reg.getDates().size() == 1);
		assertTrue(reg.getDates().iterator().next().equals(1l));
		registerDao.addEntry(ip + ".22", phoneNmber, 2l);
		reg = registerDao.getById(phoneNmber);
		assertTrue(reg.getIps().size() == 2);
		assertTrue(reg.getDates().size() == 2);
		registerDao.deleteEntry(reg);
		registerDao.cleanDates();
	}
}
