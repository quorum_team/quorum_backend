package org.chats.business.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.chats.AbstractBusinessInit;
import org.chats.db.dto.UserDto;
import org.chats.enums.UserType;
import org.junit.Test;
import org.springframework.mobile.device.DevicePlatform;

public class UsersImplTest extends AbstractBusinessInit{
	
	@Test
	public void createUser(){
		UserDto user = new UserDto();
		user.setCode("12345");
		user.setCode("972");
		user.setDevice(DevicePlatform.IOS);
		user.setName("Test User");
		user.setPhone("972123456007");
		user.setAltPhone("9720123456007");
		user.setUserType(UserType.PREMIUM);
		userBusinessLayer.addUser(user);
		verifyUser(user, true);
		try{
			userBusinessLayer.addUser(user);
		} catch(IllegalArgumentException e){
			assertTrue("User already registered".equals(e.getMessage()));
		}
		String id = user.getId();
		user.setCode("54321");
		user.setDevice(DevicePlatform.ANDROID);
		user.setName("New Test User");
		user.setUserType(UserType.REGULAR);
		userBusinessLayer.updateUser(user);
		user.setId(id);
		verifyUser(user, true);
		usersDao.removeUser(user.getId());
		verifyUser(user, false);
	}
	
	@Test
	public void userInfo(){
		UserDto currentUserInfo = userBusinessLayer.getCurrentUserInfo();
		assertTrue(ADMIN_PHONE.equals(currentUserInfo.getPhone()));
		assertTrue(UserType.PREMIUM.equals(currentUserInfo.getUserType()));
	}
	
	private void verifyUser(UserDto user, boolean exists){
		if (exists){
			Collection<UserDto> allUsers = userBusinessLayer.getAllUsers();
			verifyUser(user, allUsers);
			Collection<String> phones = userBusinessLayer.getRegisteredUsers(new ArrayList<String>(Arrays.asList(user.getPhone())));
			assertTrue(phones.contains(user.getPhone()));
			assertTrue(user.getId().equals(userBusinessLayer.getUserByPhoneAndCode(user.getPhone(), user.getCode()).getId()));
			assertTrue(userBusinessLayer.isRegistered(user.getPhone()));
		} else {
			List<UserDto> allUsers = userBusinessLayer.getAllUsers();
			assertNull(getUser(allUsers, user));
			Collection<String> phones = userBusinessLayer.getRegisteredUsers(new ArrayList<String>(Arrays.asList(user.getPhone())));
			assertFalse(phones.contains(user.getPhone()));
			assertFalse(userBusinessLayer.isRegistered(user.getPhone()));
		}
	}

	private void verifyUser(UserDto user, Collection<UserDto> allUsers) {
		UserDto actual;
		actual = getUser(allUsers, user);
		assertNotNull("User Not Found", actual);
		verifyUserProps(user, actual);
	}
	
	private UserDto getUser(Collection<UserDto> users, UserDto expected){
		UserDto result = null;
		for(UserDto user : users){
			if (user.getId().equals(expected.getId())){
				result = user;
				break;
			}
		}
		return result;
	}
	
	private void verifyUserProps(UserDto expected, UserDto actual){
		assertTrue(expected.getCode().equals(actual.getCode()));
		assertTrue(expected.getDevice().equals(actual.getDevice()));
		assertTrue(expected.getName().equals(actual.getName()));
		assertTrue(expected.getPhone().equals(actual.getPhone()));
		assertTrue(expected.getUserType().equals(actual.getUserType()));
	}
}
