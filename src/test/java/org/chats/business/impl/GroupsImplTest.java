package org.chats.business.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.chats.AbstractBusinessInit;
import org.chats.db.dto.GroupDocumentDto;
import org.chats.db.dto.RoleDto;
import org.junit.Test;

public class GroupsImplTest extends AbstractBusinessInit{
	
	@Test
	public void createGroup(){
		List<GroupDocumentDto> myGroups = groupBusinessLayer.getAllGroups();
		int count = myGroups.size();
		GroupDocumentDto group = groupBusinessLayer.createGroup("Test Group");
		myGroups = groupBusinessLayer.getAllGroups();
		int afterCount = myGroups.size();
		assertNotNull("group should have been created", group);
		assertSame("Title should be: Test Group", "Test Group", group.getTitle());
		assertSame("Groups count should have been incremented", count + 1, afterCount);
		groupBusinessLayer.removeGroup(group.getId());
		myGroups = groupBusinessLayer.getAllGroups();
		afterCount = myGroups.size();
		assertSame("Groups count should have been decremented", count, afterCount);
	}
	
	@Test
	public void checkMyGroups(){
		setLoggedInUser(USER_PHONE);
		List<GroupDocumentDto> myGroups = groupBusinessLayer.getAllGroups();
		int count = myGroups.size();
		setLoggedInUser(ADMIN_PHONE);
		GroupDocumentDto group = groupBusinessLayer.createGroup("Test Group");
		Set<String> phones = new HashSet<String>();
		phones.add(USER_PHONE);
		String id = group.getId();
		groupBusinessLayer.addUsers(id, phones);
		setLoggedInUser(USER_PHONE);
		myGroups = groupBusinessLayer.getAllGroups();
		int after = myGroups.size();
		assertTrue("Should have been added to group", after - count == 1);
		setLoggedInUser(ADMIN_PHONE);
		groupBusinessLayer.removeUser(id, USER_PHONE);
		setLoggedInUser(USER_PHONE);
		myGroups = groupBusinessLayer.getAllGroups();
		after = myGroups.size();
		assertTrue("Should have been added to group", after == count);
		setLoggedInUser(ADMIN_PHONE);
		groupBusinessLayer.removeGroup(id);
	}
	
	@Test
	public void updateGroup(){
		GroupDocumentDto group = groupBusinessLayer.createGroup("Test Group");
		assertNotNull("group should have been created", group);
		GroupDocumentDto modifiedGroup = new GroupDocumentDto();
		String id = group.getId();
		modifiedGroup.setId(id);
		modifiedGroup.setTitle("New Test Group");
		modifiedGroup.setRoles(group.getRoles());
		groupBusinessLayer.updateGroup(modifiedGroup);
		group = groupBusinessLayer.getGroupById(id);
		assertTrue("Title should be: New Test Group", "New Test Group".equals(group.getTitle()));
		groupBusinessLayer.removeGroup(id);
	}
	
	@Test
	public void removeGroup(){
		String message = "";
		try {
			groupBusinessLayer.removeGroup("111");
		} catch(IllegalArgumentException e){
			message = e.getMessage();
		}
		assertTrue("Should be thrown excpetion when trying to delete non-existing group",
				"Group Cannot be null".equals(message));
	}
	
	@Test
	public void addRemoveUsers(){
		GroupDocumentDto group = groupBusinessLayer.createGroup("Test Group");
		assertNotNull("group should have been created", group);
		int before = group.getRoles().getAuthors().size() + group.getRoles().getUsers().size();
		assertTrue("Should be an empty set of users", before == 0);
		Set<String> phones = new HashSet<String>();
		phones.add("0001112");
		String id = group.getId();
		groupBusinessLayer.addUsers(id, phones);
		group = groupBusinessLayer.getGroupById(id);
		phones = group.getRoles().getUsers();
		assertTrue("One user should have been added", phones.size() == 1);
		assertTrue("User with phone number 0001112 should have been added", phones.contains("0001112"));
		groupBusinessLayer.removeUser(id, "0001112");
		group = groupBusinessLayer.getGroupById(id);
		phones = group.getRoles().getUsers();
		assertTrue("Should be an empty set of users", phones.size() == 0);
		groupBusinessLayer.removeGroup(id);
	}
	
	@Test
	public void roles(){
		GroupDocumentDto group = groupBusinessLayer.createGroup("Test Group");
		Set<String> phones = new HashSet<String>();
		phones.add("0001112");
		String id = group.getId();
		groupBusinessLayer.addUsers(id, phones);
		group = groupBusinessLayer.getGroupById(id);
		phones = group.getRoles().getUsers();
		assertTrue("One user should have been added", phones.size() == 1);
		assertTrue("User with phone number 0001112 should have been added", phones.contains("0001112"));
		RoleDto roles = new RoleDto();
		roles.getAuthors().add(ADMIN_PHONE);
		roles.getAdmins().add("0001112");
		groupBusinessLayer.assignUserRole(id, roles);
		group = groupBusinessLayer.getGroupById(id);
		assertTrue(group.getRoles().getAdmins().size() == 2);
		assertTrue(group.getRoles().getAdmins().contains(ADMIN_PHONE));
		assertTrue(group.getRoles().getAdmins().contains("0001112"));
		assertTrue(group.getRoles().getAuthors().size() == 0);
		assertTrue(group.getRoles().getUsers().size() == 0);
		groupBusinessLayer.removeGroup(id);
	}
	
	@Test
	public void userRemovesHimself(){
		GroupDocumentDto group = groupBusinessLayer.createGroup("Test Group");
		Set<String> phones = new HashSet<String>();
		phones.add("0001112");
		phones.add(USER_PHONE);
		String id = group.getId();
		groupBusinessLayer.addUsers(id, phones);
		setLoggedInUser(USER_PHONE);
		try{
			groupBusinessLayer.removeUser(id, "0001112");
			fail("Can't remove user without admin role");
		} catch(Exception e){
		}
		groupBusinessLayer.removeUser(id, USER_PHONE);
		setLoggedInUser(ADMIN_PHONE);
		group = groupBusinessLayer.getGroupById(id);
		assertTrue(group.getRoles().getUsers().size() == 1);
		groupBusinessLayer.removeGroup(id);
	}
}
