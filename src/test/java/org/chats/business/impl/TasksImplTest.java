package org.chats.business.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.chats.AbstractBusinessInit;
import org.chats.db.dto.GroupDocumentDto;
import org.chats.db.dto.TaskDto;
import org.chats.db.dto.TaskItemDto;
import org.chats.enums.TaskType;
import org.junit.Test;

public class TasksImplTest extends AbstractBusinessInit{
	
	@Test
	public void crud(){
		long currentTimeMillis = System.currentTimeMillis();
		GroupDocumentDto createGroup = groupBusinessLayer.createGroup("Test Group");
		String groupId = createGroup.getId();
		TaskDto createTask = taskBusinessLayer.createTask(groupId, "Test Task", TaskType.ATTENDING, null, null, null);
		String taskId = createTask.getId();
		createGroup = groupBusinessLayer.getGroupById(groupId);
		TaskDto foundTask = findTask(taskId,createGroup);
		assertNotNull(foundTask);
		assertTrue("Test Task".equals(foundTask.getTaskName()));
		assertTrue(foundTask.getCreatedAt() > currentTimeMillis);
		assertTrue(foundTask.getType() == TaskType.ATTENDING);
		assertTrue(foundTask.getItems().size() == 0);
		List<String> answers = new ArrayList<String>();
		answers.add("answer1");
		answers.add("answer2");
		foundTask.setAnswers(answers);
		taskBusinessLayer.modifyTask(foundTask, groupId);
		taskBusinessLayer.addTaskItem(groupId, taskId, "New Item", ADMIN_PHONE, "");
		createGroup = groupBusinessLayer.getGroupById(groupId);
		foundTask = findTask(taskId, createGroup);
		assertNotNull(foundTask);
		TaskItemDto taskItem = foundTask.getItems().iterator().next();
		try{
			taskBusinessLayer.changeTaskItemState(groupId, taskId, taskItem.getId(), "s");
			fail("Can't add a non valid answer");
		} catch(Exception e){}
		taskBusinessLayer.changeTaskItemState(groupId, taskId, taskItem.getId(), "");
		taskBusinessLayer.changeTaskItemState(groupId, taskId, taskItem.getId(), "answer1");
		createGroup = groupBusinessLayer.getGroupById(groupId);
		foundTask = findTask(taskId, createGroup);
		assertNotNull(foundTask);
		taskItem = foundTask.getItems().iterator().next();
		assertNotNull(taskItem);
		assertTrue("New Item".equals(taskItem.getTitle()));
		assertTrue(taskItem.getUpdateAt() > currentTimeMillis);
		assertTrue(ADMIN_PHONE.equals(taskItem.getUserId()));
		assertTrue("answer1".equals(taskItem.getAnswer()));
		taskBusinessLayer.removeTaskItem(groupId, taskId, taskItem.getId());
		createGroup = groupBusinessLayer.getGroupById(groupId);
		foundTask = findTask(taskId, createGroup);
		assertNotNull(foundTask);
		assertTrue(foundTask.getItems().size() == 0); 
		taskBusinessLayer.removeTask(groupId, taskId);
		createGroup = groupBusinessLayer.getGroupById(groupId);
		foundTask = findTask(taskId, createGroup);
		assertNull(foundTask);
		groupBusinessLayer.removeGroup(groupId);
	}
	
	private TaskDto findTask(String taskId, GroupDocumentDto group){
		for(TaskDto task : group.getTasks()){
			if (taskId.equals(task.getId())){
				return task;
			}
		}
		return null;
	}
}
