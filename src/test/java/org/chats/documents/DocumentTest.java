package org.chats.documents;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.chats.db.dto.GroupDocumentDto;
import org.chats.db.dto.TaskDto;
import org.junit.Before;
import org.junit.Test;

public class DocumentTest {

	private GroupDocumentDto group;
	
	private Document document;
	
	@Before
	public void init(){
		group = new GroupDocumentDto();
		document = Document.builder(group);
	}
	
	@Test
	public void setAuthor(){
		document.setAuthor("123");
		assertEquals(true, document.isAuthor("123"));
	}
	
	@Test
	public void setAdmin(){	
		document.setAdmin("054");
		assertEquals(true, document.isAdmin("054"));
		document.setUser("054");
		assertEquals(false, document.isAdmin("054"));
	}
	
	@Test
	public void fetchTask(){
		TaskDto task = new TaskDto();
		task.setId("123");
		task.setTaskName("Test Name");
		document.addTask(task);
		TaskDto taskById = document.getTaskById("123");
		assertEquals("Test Name", taskById.getTaskName());
		document.removeTask("123");
		assertNull(document.getTaskById("123"));
	}
}
