package org.chats.utils;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class CodeGeneratorTest {

	@Test
	public void test() {
		Set<String> codes = new HashSet<String>();
		CodeGenerator cg = new CodeGenerator();
		for(int i = 0; i < 50; i++){
			String str = cg.getCode();
			codes.add(str);
			assertEquals("Code should be 4 digits length", 5, str.length());
		}
		assertEquals("100 unique codes should have been created", 50, codes.size()); 
	}

}
