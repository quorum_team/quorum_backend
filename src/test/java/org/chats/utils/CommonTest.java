package org.chats.utils;

import static org.chats.utils.Common.isNull;
import static org.chats.utils.Common.nullAwareBeanCopy;

import java.util.ArrayList;
import java.util.List;

import org.chats.db.dto.TaskDto;
import org.junit.Test;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertTrue;

public class CommonTest {
	
	@Test
	public void copyObjects(){
		TaskDto taskDto = new TaskDto();
		TaskDto taskDto2 = new TaskDto();
		taskDto.setTaskName("Task1");
		taskDto2.setCreatedBy("123");
		try{
			nullAwareBeanCopy((Object)taskDto, (Object)taskDto2);	
		} catch(Exception e){
			fail("Object copy failed");
		}
		assertTrue("Task name should be: Task1", "Task1".equals(taskDto.getTaskName()));
		assertTrue("Task group should be: 123", "123".equals(taskDto.getCreatedBy()));
	}

	@Test
	public void isNullStr(){
		assertTrue("".equals(isNull(null, "")));
		assertTrue("".equals(isNull("", "abc")));
		assertTrue("abc".equals(isNull(null, "abc")));
		assertTrue("abc".equals(isNull("abc", "abcd")));
	}
	
	@Test
	public void phones(){
		List<String> phones = new ArrayList<String>(4);
		phones.add(" 01213123");
		phones.add("+121-528138526");
		phones.add("122-054-52024-79");
		phones.add("(057) 999 95 95");
		List<String> normalizedPhonesList = Common.getNormalizedPhonesList("121", phones);
		int count = 0;
		if (normalizedPhonesList.contains("1211213123")){
			count++;
		}
		if (normalizedPhonesList.contains("121528138526")){
			count++;
		}
		if (normalizedPhonesList.contains("1220545202479")){
			count++;
		}
		if (normalizedPhonesList.contains("121579999595")){
			count++;
		}
		assertTrue("Some phone numbers weren't found", count == 4);
	}
}
