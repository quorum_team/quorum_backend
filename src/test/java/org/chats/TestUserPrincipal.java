package org.chats;

import java.security.Principal;

public class TestUserPrincipal implements Principal{
	
	String userPhone;
	
	public TestUserPrincipal(String userPhone) {
		this.userPhone = userPhone;
	}
	
	public String getName() {
		return userPhone;
	}
}
