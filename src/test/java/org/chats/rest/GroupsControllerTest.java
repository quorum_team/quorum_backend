package org.chats.rest;

import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.chats.AbstractRestInit;
import org.chats.db.dto.GroupDocumentDto;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.StringUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

public class GroupsControllerTest extends AbstractRestInit {
	
	public final static String GROUPS = "/groups";
	public final static String CREATE_GROUP = GROUPS + "/group";
	public final static String GROUPS_INSTANCE = GROUPS + "/%s";
	private final static String GROUPS_USERS = GROUPS + "/users/%s";
	private final static String GROUPS_DELETE_USER = GROUPS_USERS + "/%s";

	@Test
	public void groupCreate() throws Exception{
		MvcResult result = postCall(CREATE_GROUP, "{\"title\": \"Test Group\"}", true, HttpStatus.OK, false);
		GroupDocumentDto group = resolveResult(result, "groups", GroupDocumentDto.class);
		Assert.assertTrue("Group Title should be: Test Group", "Test Group".equals(group.getTitle()));
		Assert.assertTrue("Group Users list should be empty", group.getRoles().getUsers().size() == 0);
		Assert.assertTrue("Group Creator should doen't match", ADMIN_PHONE.equals(group.getRoles().getAdmins().iterator().next()));
		deleteCall(String.format(GROUPS_INSTANCE, group.getId()), true, HttpStatus.OK, false);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void groupModify() throws Exception{
		List<GroupDocumentDto> groups = resolveResult(getCall(GROUPS, true, HttpStatus.OK, false), "groups", List.class);
		int before = groups.size();
		GroupDocumentDto group = new GroupDocumentDto();
		group.setTitle("Test Group");
		group = resolveResult(postCall(CREATE_GROUP, groupDtoJson(group), true, HttpStatus.OK, false), "groups", GroupDocumentDto.class);
		groups = resolveResult(getCall(GROUPS, true, HttpStatus.OK, false), "groups", List.class);
		int after = groups.size();
		Assert.assertTrue("My groups collection size should grow by one", after - before == 1);
		group.setTitle("New Test Group");
		putCall(GROUPS, groupDtoJson(group), true, HttpStatus.OK, true);
		groups = resolveResult(getCall(GROUPS, true, HttpStatus.OK, false), "groups", List.class);
		String groupId = group.getId();
		group = findGroup(groups, groupId);
		Assert.assertTrue("Group wasn't found", group != null);
		assertThat("Group title wasn't changed", group.getTitle(), equalTo("New Test Group"));
		JsonArray ja = new JsonArray();
		ja.add(new JsonPrimitive(USER_PHONE));
		postCall(String.format(GROUPS_USERS, groupId), ja.toString(), true, HttpStatus.OK, false);
		groups = resolveResult(getCall(GROUPS, true, HttpStatus.OK, false), "groups", List.class);
		setLogInUser(USER_PHONE);
		group = findGroup(groups, groupId);
		assertThat("User Have Should be add", group.getRoles().getUsers(), hasItem(USER_PHONE));
		setLogInUser(ADMIN_PHONE);
		ja = new JsonArray();
		ja.add(new JsonPrimitive("9720589898877"));
		postCall(String.format(GROUPS_USERS, groupId), ja.toString(), true, HttpStatus.OK, false);
		setLogInUser(USER_PHONE);
		groups = resolveResult(getCall(GROUPS, true, HttpStatus.OK, false), "groups", List.class);
		group = findGroup(groups, groupId);
		assertThat("User Have Should be add", group.getRoles().getUsers(), hasItem(USER_PHONE));
		setLogInUser(ADMIN_PHONE);
		JsonObject rolesObj = new JsonObject();
		JsonArray authors = new JsonArray();
		authors.add(new JsonPrimitive(USER_PHONE));
		rolesObj.add("authors", authors);
		putCall(String.format(GROUPS_USERS, groupId), rolesObj.toString(), true, HttpStatus.OK, false);
		groups = resolveResult(getCall(GROUPS, true, HttpStatus.OK, false), "groups", List.class);
		group = findGroup(groups, groupId);
		assertThat("User Should have been added as an Author", group.getRoles().getAuthors(), hasItem(USER_PHONE));
		assertThat("User Should have been added as an Author", group.getRoles().getUsers(), not(hasItem(USER_PHONE)));
		
		deleteCall(String.format(GROUPS_DELETE_USER, groupId, USER_PHONE), true, HttpStatus.OK, false);
		groups = resolveResult(getCall(GROUPS, true, HttpStatus.OK, false), "groups", List.class);
		group = findGroup(groups, groupId);
		assertThat("User Have Should be add", group.getRoles().getUsers(), not(hasItem(USER_PHONE)));
		deleteCall(String.format(GROUPS_INSTANCE, groupId), true, HttpStatus.OK, false);
		groups = resolveResult(getCall(GROUPS, true, HttpStatus.OK, false), "groups", List.class);
		group = findGroup(groups, groupId);
		Assert.assertTrue("Group wasn't removed", group == null);
	}
	
	private String groupDtoJson(GroupDocumentDto group){
		JsonObject jsonObject = new JsonObject();
		if (!StringUtils.isEmpty(group.getTitle())){
			jsonObject.add("title", new JsonPrimitive(group.getTitle()));
		}
		if (!StringUtils.isEmpty(group.getId())){
			jsonObject.add("id", new JsonPrimitive(group.getId()));
		}
		
		if (!StringUtils.isEmpty(group.getRoles().getUsers())){
			JsonObject users = new JsonObject();
			JsonArray ja = new JsonArray();
			for(String user : group.getRoles().getUsers()){
				ja.add(new JsonPrimitive(user));
			}
			users.add("users", ja); 
			jsonObject.add("roles", users);
		}
		if (!StringUtils.isEmpty(group.getRoles().getAdmins())){
			JsonObject admins = new JsonObject();
			JsonArray ja = new JsonArray();
			for(String user : group.getRoles().getAdmins()){
				ja.add(new JsonPrimitive(user));
			}
			admins.add("admins", ja); 
			jsonObject.add("roles", admins);
		}
		
		return jsonObject.toString();
	}
	
	private GroupDocumentDto findGroup(List<GroupDocumentDto> groups, String id){
		for(GroupDocumentDto groupDto : groups){
			if (groupDto.getId().equals(id)){
				return groupDto;
			}
		}
		return null;
	}
}
