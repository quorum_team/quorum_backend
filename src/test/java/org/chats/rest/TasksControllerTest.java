package org.chats.rest;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.chats.AbstractRestInit;
import org.chats.db.dto.GroupDocumentDto;
import org.chats.db.dto.TaskDto;
import org.chats.db.dto.TaskItemDto;
import org.chats.enums.TaskType;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.StringUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

public class TasksControllerTest extends AbstractRestInit {
	
	private final static String TASKS = "/tasks/%s";
	private final static String TASKS_DELETE = TASKS + "/%s";
	private final static String TASKS__ITEM_DELETE = TASKS_DELETE + "/%s";
	private final static String TASK_ITEM_STATE = TASKS__ITEM_DELETE + "/%s";
	
	@SuppressWarnings("unchecked")
	@Test
	public void crud() throws Exception{
		JsonObject jsonObject = newTaskJson();
		// Negative test for creating task to non existing group
		postCall(String.format(TASKS, "121"), jsonObject.toString(), true, HttpStatus.INTERNAL_SERVER_ERROR, false);
		// Create test group
		MvcResult result = postCall(GroupsControllerTest.CREATE_GROUP, "{\"title\": \"Test Group\"}", true, HttpStatus.OK, false);
		GroupDocumentDto testGroup = resolveResult(result, "groups", GroupDocumentDto.class);
		// Test create task, first request is negative
		String testGroupId = testGroup.getId();
		MvcResult postCall = postCall(String.format(TASKS, testGroupId), jsonObject.toString(), true, HttpStatus.OK, true);
		TaskDto testTask = resolveResult(postCall, "tasks", TaskDto.class);
		assertThat(testTask.getTaskName(), equalTo("Test Task"));
		assertTrue(testTask.getCreatedAt() > 0);
		assertTrue(testTask.getItems().size() == 0);
		assertTrue(!StringUtils.hasText(testTask.getProperties()));
		assertTrue(testTask.getAnswers().size() == 0);
		assertTrue(testTask.getType().equals(TaskType.CHECKLIST));
		assertTrue(testTask.getUpdatedAt() == testTask.getCreatedAt());
		// Get Tasks
		List<TaskDto> tasks = resolveResult(getCall(String.format(TASKS, testGroup.getId()), true, HttpStatus.OK, false), "tasks", List.class);
		String testTaskId = testTask.getId();
		testTask = findTask(tasks, testGroupId, testTaskId);
		assertThat(testTask.getTaskName(), equalTo("Test Task"));
		assertTrue(testTask.getCreatedAt() > 0);
		assertTrue(testTask.getItems().size() == 0);
		assertTrue(testTask.getUpdatedAt() == testTask.getCreatedAt());
		
		// Remove Task
		deleteCall(String.format(TASKS_DELETE, testGroupId, testTaskId), true, HttpStatus.OK, false);	
		// Verify task was deleted
		tasks = resolveResult(getCall(String.format(TASKS, testGroup.getId()), true, HttpStatus.OK, false), "tasks", List.class);
		testTask = findTask(tasks, testGroupId, testTaskId);
		assertNull(testTask);
		deleteCall(String.format(GroupsControllerTest.GROUPS_INSTANCE, testGroupId), true, HttpStatus.OK, false);
	}
	
	@Test
	public void regularUser() throws Exception{
		setLogInUser(USER_PHONE);
		JsonObject jsonObject = newTaskJson();
		MvcResult result = postCall(GroupsControllerTest.CREATE_GROUP, "{\"title\": \"Test Group\"}", true, HttpStatus.OK, false);
		GroupDocumentDto testGroup = resolveResult(result, "groups", GroupDocumentDto.class);
		MvcResult postCall = postCall(String.format(TASKS, testGroup.getId()), jsonObject.toString(), true, HttpStatus.OK, true);
		TaskDto testTask = resolveResult(postCall, "tasks", TaskDto.class);
		MvcResult result2 = postCall(GroupsControllerTest.CREATE_GROUP, "{\"title\": \"Test Group\"}", true, HttpStatus.OK, false);
		GroupDocumentDto testGroup2 = resolveResult(result2, "groups", GroupDocumentDto.class);
		postCall(String.format(TASKS, testGroup2.getId()), jsonObject.toString(), true, HttpStatus.INTERNAL_SERVER_ERROR, true);
		deleteCall(String.format(TASKS_DELETE, testGroup.getId(), testTask.getId()), true, HttpStatus.OK, false);
		deleteCall(String.format(GroupsControllerTest.GROUPS_INSTANCE, testGroup.getId()), true, HttpStatus.OK, false);
		deleteCall(String.format(GroupsControllerTest.GROUPS_INSTANCE, testGroup2.getId()), true, HttpStatus.OK, false);
	}
	
	@Test
	public void addAnswer() throws Exception{
		JsonObject jsonObject = newTaskJson();
		MvcResult result = postCall(GroupsControllerTest.CREATE_GROUP, "{\"title\": \"Test Group\"}", true, HttpStatus.OK, false);
		GroupDocumentDto testGroup = resolveResult(result, "groups", GroupDocumentDto.class);
		String groupId = testGroup.getId();
		MvcResult postCall = postCall(String.format(TASKS, groupId), jsonObject.toString(), true, HttpStatus.OK, true);
		TaskDto testTask = resolveResult(postCall, "tasks", TaskDto.class);
		String testTaskId = testTask.getId();
		List<String> answers = new ArrayList<String>();
		answers.add("answer1");
		answers.add("answer2");
		testTask.setAnswers(answers);
		putCall(String.format(TASKS, groupId), taskToJson(testTask).toString(), true, HttpStatus.OK, true);
		postCall(String.format(TASKS_DELETE, groupId, testTask.getId()), "{\"title\": \"test\", \"userId\": \"\"}",
				true, HttpStatus.OK, true);
		TaskItemDto item = getItem(groupId, testTaskId);
		assertTrue(item.getTitle().equals("test"));
		assertTrue(item.getUserId().equals(""));
		deleteCall(String.format(TASKS__ITEM_DELETE, groupId, testTaskId, item.getId()), true, HttpStatus.OK, true);

		postCall(String.format(TASKS_DELETE, groupId, testTaskId), "{\"title\": \"test\", \"userId\": \"3423423\"}",
				true, HttpStatus.INTERNAL_SERVER_ERROR, true);
		postCall(String.format(TASKS_DELETE, groupId, testTaskId), "{\"title\": \"test\", \"userId\": \"" + ADMIN_PHONE + "\"}",
				true, HttpStatus.OK, true);
		item = getItem(groupId, testTaskId);
		assertTrue(item.getTitle().equals("test"));
		assertTrue(item.getUserId().equals(ADMIN_PHONE));
		putCall(String.format(TASK_ITEM_STATE, groupId, testTaskId, item.getId(), ""), "", true, HttpStatus.OK, true);
		item = getItem(groupId, testTaskId);
		assertTrue(item.getAnswer().equals(""));
		putCall(String.format(TASK_ITEM_STATE, groupId, testTaskId, item.getId(), "abc"), "", true, HttpStatus.INTERNAL_SERVER_ERROR, true);
		putCall(String.format(TASK_ITEM_STATE, groupId, testTaskId, item.getId(), "answer1"), "", true, HttpStatus.OK, true);
		item = getItem(groupId, testTaskId);
		assertTrue(item.getAnswer().equals("answer1"));
		deleteCall(String.format(TASKS_DELETE, testGroup.getId(), testTask.getId()), true, HttpStatus.OK, false);
		deleteCall(String.format(GroupsControllerTest.GROUPS_INSTANCE, testGroup.getId()), true, HttpStatus.OK, false);
	}
	
	@SuppressWarnings("unchecked")
	private TaskItemDto getItem(String groupId, String testTaskId) throws Exception{
		List<TaskDto> tasks = resolveResult(getCall(String.format(TASKS, groupId), true, HttpStatus.OK, false), "tasks", List.class);
		TaskDto testTask = findTask(tasks, groupId, testTaskId);
		assertTrue(testTask.getItems().size() == 1);
		return testTask.getItems().iterator().next();
	}

	private JsonObject newTaskJson() {
		JsonObject jsonObject = new JsonObject();
		jsonObject.add("taskName", new JsonPrimitive("Test Task"));
		jsonObject.add("type", new JsonPrimitive("CHECKLIST"));
		return jsonObject;
	}
	
	private JsonObject taskToJson(TaskDto task){
		JsonObject jsonObject = new JsonObject();
		JsonArray jsonArray = new JsonArray();
		for(String answer : task.getAnswers()){
			jsonArray.add(new JsonPrimitive(answer));
		}
		jsonObject.add("answers", jsonArray);
		jsonObject.add("createdAt", new JsonPrimitive(task.getCreatedAt()));
		jsonObject.add("id", new JsonPrimitive(task.getId()));
		if (StringUtils.hasText(task.getProperties())){
			jsonObject.add("properties", new JsonPrimitive(task.getProperties()));
		}
		jsonObject.add("taskName", new JsonPrimitive(task.getTaskName()));
		jsonObject.add("updatedAt", new JsonPrimitive(task.getUpdatedAt()));
		jsonObject.add("type", new JsonPrimitive(task.getType().name()));
		jsonArray = new JsonArray();
		for(TaskItemDto item : task.getItems()){
			JsonObject jsonItemObject = new JsonObject();
			jsonItemObject.add("answer", new JsonPrimitive(item.getAnswer()));
			jsonItemObject.add("id", new JsonPrimitive(item.getId()));
			jsonItemObject.add("title", new JsonPrimitive(item.getTitle()));
			jsonItemObject.add("userId", new JsonPrimitive(item.getUserId()));
			jsonItemObject.add("updateAt", new JsonPrimitive(item.getUpdateAt()));
			jsonArray.add(jsonItemObject);
		}
		jsonObject.add("items", jsonArray);
		return jsonObject;
	}
	
	private TaskDto findTask(List<TaskDto> tasks, String groupId, String taskId){
		for(TaskDto task : tasks){
			if (task.getId().equals(taskId)){
				return task;
			}
		}
		return null;
	}
}
