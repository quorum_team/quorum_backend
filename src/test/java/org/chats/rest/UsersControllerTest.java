package org.chats.rest;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.chats.AbstractRestInit;
import org.chats.db.dto.UserDto;
import org.chats.enums.UserType;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MvcResult;

public class UsersControllerTest extends AbstractRestInit{
	
	private final static String GET_USERS = "/users";
	private final static String REGISTER = "/register"; 

	@SuppressWarnings("unchecked")
	@Test
	public void getUsers() throws Exception {
		MvcResult result = getCall(GET_USERS, true, HttpStatus.OK, false);
		List<UserDto> users = resolveResult(result, "users", List.class);
		boolean userFound = false;
		for(UserDto user : users){
			if (ADMIN_PHONE.equals(user.getPhone())){
				userFound = true;
				break;
			}
		}
		assertTrue("User not found", userFound);
	}

	@Test
	public void registerUser() throws Exception{
		postCall(REGISTER, "{\"countryExt\": \"972\", \"phone\": \"0539991122\"}", false, HttpStatus.OK, false);
		UserDto userByPhone = usersDao.getUserByPhone("0539991122");
		assertTrue(userByPhone != null);
		usersDao.removeUser(userByPhone.getId());
	}
	
	@Test
	public void userInfo() throws Exception{
		UserDto resolveResult = resolveResult(getCall(GET_USERS + "/info", true, HttpStatus.OK, true), "info", UserDto.class);
		assertTrue(ADMIN_PHONE.equals(resolveResult.getPhone()));
		assertTrue(UserType.PREMIUM.equals(resolveResult.getUserType()));
	}
	
}