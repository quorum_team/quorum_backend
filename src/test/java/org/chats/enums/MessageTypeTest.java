package org.chats.enums;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

public class MessageTypeTest {
	
	@Test
	public void convert(){
		assertTrue("Should be TEXT enum", MessageType.getEnumValue("TEXT").equals(MessageType.TEXT));
		assertTrue("Should be IMAGE enum", MessageType.getEnumValue("IMAGE").equals(MessageType.IMAGE));
		try{
			 MessageType.getEnumValue("xxx");
			 fail("Should fail on non existing enum value");
		} catch(Exception e){}
	}

}

