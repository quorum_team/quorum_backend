package org.chats;

import java.util.HashMap;
import java.util.Map;

import org.chats.dao.GroupDocumentsDao;
import org.chats.dao.MessagesDao;
import org.chats.dao.RegistrationDao;
import org.chats.dao.UsersDao;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "file:src/main/webapp/WEB-INF/security.xml")
@Ignore
public class AbstractDaoInit {
	
	@Autowired
	protected GroupDocumentsDao groupsDao;
	
	@Autowired
	protected UsersDao usersDao;
	
	@Autowired
	protected RegistrationDao registerDao;
	
	@Autowired
	protected MessagesDao messagesDao;
	
	@Autowired
	@InjectMocks
	protected GroupDocumentsDao groupDocumentsDao;
	
	protected final static String USER_PHONE = "000222";
	protected final static String ADMIN_PHONE = "000111";
	private static Map<String, String> users = new HashMap<String, String>();
	
	static {
		
		users.put("000111", "91808");
		users.put("000222", "06198");
	}
	
	public String getUserPassword(String userPhone){
		return users.get(userPhone);
	}

}
