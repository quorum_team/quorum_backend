package org.chats;

import org.chats.business.Groups;
import org.chats.business.Tasks;
import org.chats.business.Users;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@ContextConfiguration(locations = "file:src/main/resources/application-context.xml")
@Ignore
public class AbstractBusinessInit extends AbstractDaoInit{
	
	@Autowired
	protected Groups groupBusinessLayer;
	
	@Autowired
	protected Users userBusinessLayer;
	
	@Autowired
	protected Tasks taskBusinessLayer;

	@BeforeClass
	public static void init(){
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setUserPrincipal(new TestUserPrincipal(ADMIN_PHONE));
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
	}

	protected void setLoggedInUser(String userid){
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setUserPrincipal(new TestUserPrincipal(userid));
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
	}
}
