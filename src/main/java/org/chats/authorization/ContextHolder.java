package org.chats.authorization;

import java.util.HashSet;
import java.util.Set;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.socket.messaging.AbstractSubProtocolEvent;

public class ContextHolder {
	
	private static Set<String> connectedPhones = new HashSet<String>();
	
	public String getLoggedUserPhone(){
		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		return attributes.getRequest().getUserPrincipal().getName();
	}
	
	public void removeUserId(){
    	ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
    	attributes.getRequest().getSession().invalidate();
	}
	
	public void addSessionPhone(AbstractSubProtocolEvent wsSession){
		connectedPhones.add(wsSession.getUser().getName());
	}
	
	public void removeSessionPhone(AbstractSubProtocolEvent wsSession){
		connectedPhones.remove(wsSession.getUser().getName());
	}
	
	public Set<String> getConnectedPhones(){
		return connectedPhones;				
	}
}
