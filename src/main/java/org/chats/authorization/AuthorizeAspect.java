package org.chats.authorization;

import java.lang.annotation.Annotation;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.chats.dao.GroupDocumentsDao;
import org.chats.db.dto.GroupDocumentDto;
import org.chats.documents.Document;
import org.chats.enums.Roles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

@Aspect
public class AuthorizeAspect {
	
	@Autowired
	ContextHolder contextHolder;
	
	@Autowired
	private GroupDocumentsDao groupsDao;

	@Before("execution(* *.*(..)) && @annotation(authorize) ")
	public void authorize(JoinPoint joinPoint, Authorize authorize) throws Throwable {
		int argInd = authorize.parameter();
		Roles type = authorize.type();
		if (argInd > -1){
			GroupDocumentDto group = null;
			if (joinPoint.getArgs()[argInd] instanceof String){
				String groupId = (String)joinPoint.getArgs()[argInd];
				group = groupsDao.getById(groupId);
			} else {
				group = (GroupDocumentDto)joinPoint.getArgs()[argInd];
				group = groupsDao.getById(group.getId());
			}

    		Document builder = Document.builder(group);
    		Assert.notNull(group, "No such group");
    		String userPhone = getLogedUserPhone(joinPoint, group.getCreatedBy());
    		if (type.equals(Roles.ADMIN)){
    			if (!builder.isAdmin(userPhone)){
    				throw new SecurityException("You have to be an admin of this group");
    			}
    		}
    		
    		if (type.equals(Roles.AUTHOR)){
    			if (!builder.isAdmin(userPhone) && !builder.isAuthor(userPhone)){
    				throw new SecurityException("You have to be at least an author of this group");
    			}
    		}
    		
    		if (type.equals(Roles.USER)){
    			if (!builder.isAdmin(userPhone) && !builder.isAuthor(userPhone) && !builder.isUser(userPhone)){
    				throw new SecurityException("Are you sure you're a member of this group?");
    			}
    		}
		}
	}
	
	public String getLogedUserPhone(JoinPoint joinPoint, String creator) throws NoSuchMethodException, SecurityException{
		MethodSignature signature = (org.aspectj.lang.reflect.MethodSignature) joinPoint.getSignature();
		String methodName = signature.getMethod().getName();
		Class<?>[] parameterTypes = signature.getMethod().getParameterTypes();
		Annotation[][] annotations = joinPoint.getTarget().getClass().getMethod(methodName,parameterTypes).getParameterAnnotations();
		String userPhone = "";
		int paramInd = 0;
		for(Annotation[] annotationsArr : annotations){
			if (annotationsArr.length > 0){
				if (annotationsArr[0].annotationType().equals(Authorize.class)){
					Authorize paramAnnotation = (Authorize)annotationsArr[0];
					if (joinPoint.getArgs()[paramInd] instanceof String){
						userPhone = (String)joinPoint.getArgs()[paramInd];
						if (paramAnnotation.parameter() == 0){
							userPhone = userPhone.equals(contextHolder.getLoggedUserPhone()) ? creator : contextHolder.getLoggedUserPhone(); 
						} 
						break;
					}
				}
			}
			paramInd++;
		}
		return StringUtils.hasLength(userPhone) ? userPhone : contextHolder.getLoggedUserPhone();
	}
}
