package org.chats.authorization;

import java.util.HashSet;
import java.util.Set;

import org.chats.dao.UsersDao;
import org.chats.db.dto.UserDto;
import org.chats.enums.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class UsersDbDetailsService implements UserDetailsService {

	@Autowired
	private UsersDao usersDao;
	
	public UserDetails loadUserByUsername(String userPhone) throws UsernameNotFoundException {
		UserDto dbUser = usersDao.getUserByPhone(userPhone);
		if (dbUser == null) {
			throw new UsernameNotFoundException("User not found");
		}
		Set<GrantedAuthority> roles = new HashSet<GrantedAuthority>();
		roles.add(new SimpleGrantedAuthority("ROLE_" + dbUser.getUserType()));
		if (dbUser.getUserType().equals(UserType.PREMIUM)){
			roles.add(new SimpleGrantedAuthority("ROLE_" + UserType.REGULAR));
		}
		return new User(userPhone, dbUser.getCode(), roles);
	}

}
