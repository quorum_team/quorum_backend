package org.chats.business;

import java.util.List;
import java.util.Set;

import org.chats.db.dto.GroupDocumentDto;
import org.chats.db.dto.RoleDto;

public interface Groups {

	public GroupDocumentDto createGroup(String title);
	
	public void removeGroup(String groupId);
	
	public void addUsers(String groupId, Set<String> userIds);
	
	public void removeUser(String groupId, String phone);
	
	public List<GroupDocumentDto> getAllGroups(String... userPhone);
	
	public GroupDocumentDto getGroupById(String groupId, String... userId);
	
	public void updateGroup(GroupDocumentDto group);
	
	public void assignUserRole(String groupId, RoleDto roles);
}
