package org.chats.business;

import org.chats.db.dto.MessageContentDto;

public interface Messages {

	public MessageContentDto pushMessage(MessageContentDto message, String userId, String groupId);
	
	public void pullMessage(String phone);
	
	public void pullMessage(MessageContentDto messageContentDto);
	
	public void removeMessage(String messageId, String userId);
}
