package org.chats.business;

import java.util.List;
import java.util.Set;

import org.chats.db.dto.UserDto;

public interface Users {

	public UserDto addUser(UserDto userDto);
	
	public List<UserDto> getAllUsers();
	
	public UserDto getUserByPhoneAndCode(String phone, String code);
	
	public Set<String> getRegisteredUsers(List<String> phones);
	
	public void updateUser(UserDto user);
	
	public boolean isRegistered(String phone);
	
	public UserDto getCurrentUserInfo();
}
