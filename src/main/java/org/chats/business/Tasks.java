package org.chats.business;

import java.util.List;
import java.util.Set;

import org.chats.db.dto.GroupDocumentDto;
import org.chats.db.dto.TaskDto;
import org.chats.db.dto.TaskItemDto;
import org.chats.enums.TaskType;

public interface Tasks {
	
	public TaskDto createTask(String groupId, String taskName, TaskType type, String properties, List<String> answers, Set<TaskItemDto> items);
	
	public void removeTask(String groupId, String taskId);
	
	public GroupDocumentDto addTaskItem(String groupId, String taskId, String itemTitle, String userId, String answer);
	
	public GroupDocumentDto removeTaskItem(String groupId, String taskId, String itemId);
	
	public void modifyTask(TaskDto taskDto, String groupId);
	
	public GroupDocumentDto changeTaskItemState(String groupId, String taskId, String itemId, String answer);
}
