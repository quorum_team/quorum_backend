package org.chats.business;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.chats.enums.UserType;

import com.dropbox.core.DbxException;
import com.dropbox.core.v2.DbxFiles.GetMetadataException;

public interface Icons {

	public Map<UserType, List<String>> iconsList() throws DbxException;
	
	public void downloadPublicFile(String iconPath, HttpServletResponse response);
	
	public void downloadPremiumFile(String iconPath, HttpServletResponse response);
	
	public String getPublicFile(String iconPath) throws GetMetadataException, DbxException, IOException;
	
	public String getPremiumFile(String iconPath) throws GetMetadataException, DbxException, IOException;
}
