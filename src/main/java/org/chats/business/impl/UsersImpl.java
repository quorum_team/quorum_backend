package org.chats.business.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.chats.authorization.ContextHolder;
import org.chats.business.Users;
import org.chats.dao.UsersDao;
import org.chats.db.dto.UserDto;
import org.chats.utils.Common;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

public class UsersImpl implements Users {
	
	@Autowired
	UsersDao usersDao;
	
	@Autowired
	private ContextHolder contextHolder;
		
	public UserDto addUser(UserDto userDto) {
		UserDto user = usersDao.getUserByPhone(userDto.getPhone());
		if (user == null){
			usersDao.addUser(userDto);
			user = userDto;
		}
		
		return user;
	}

	public List<UserDto> getAllUsers()
    {
		return usersDao.getAllUsers();
	}

	public UserDto getUserByPhoneAndCode(String phone, String code) {
		return usersDao.getUserByPhoneAndCode(phone, code);
	}

	public Set<String> getRegisteredUsers(List<String> phones) {
		Set<String> existingPhones = new HashSet<String>();
		String countryExt = usersDao.getUserByPhone(contextHolder.getLoggedUserPhone()).getCountryExt();
		for(String phone : phones){
			if (usersDao.isUserExists(Common.getNormalizedPhone(countryExt, phone))){
				existingPhones.add(phone);
			}
		}
		return existingPhones;
	}
	
	public boolean isRegistered(String phone) {
		return usersDao.getUserByPhone(phone) != null;
	}

	public void updateUser(UserDto user) {
		String phone = user.getPhone();
		Assert.notNull(phone = StringUtils.isEmpty(phone) ? null : phone, "Phone number should be provided!");
		UserDto dbUser = usersDao.getUserByPhone(phone);
		Assert.notNull(dbUser, "No user was found with phone: " + phone);
		user.setId(null);
		updateUserDto(user, dbUser);
		usersDao.updateUser(dbUser);
	}
	
	private void updateUserDto(UserDto user, UserDto toUpdate){
		try{
			Common.nullAwareBeanCopy(toUpdate, user);
		}
		catch(Exception e){
			throw new IllegalArgumentException("There was a general error to update the user");
		}
	}
	
	public UserDto getCurrentUserInfo(){
		String loggedUserPhone = contextHolder.getLoggedUserPhone();
		UserDto userByPhone = usersDao.getUserByPhone(loggedUserPhone);
		UserDto infoDto = new UserDto();
		infoDto.setId(null);
		infoDto.setName(userByPhone.getName());
		infoDto.setPhone(loggedUserPhone);
		infoDto.setUserType(userByPhone.getUserType());
		return infoDto;
	}
}
