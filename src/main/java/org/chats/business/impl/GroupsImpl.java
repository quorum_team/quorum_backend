package org.chats.business.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.chats.authorization.Authorize;
import org.chats.authorization.ContextHolder;
import org.chats.business.Groups;
import org.chats.dao.GroupDocumentsDao;
import org.chats.db.dto.GroupDocumentDto;
import org.chats.db.dto.RoleDto;
import org.chats.documents.Document;
import org.chats.enums.Roles;
import org.chats.event.MessageDecorator;
import org.chats.utils.Common;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

public class GroupsImpl extends MessageDecorator implements Groups{
	
	@Autowired
	private ContextHolder contextHolder;
	
	@Autowired
	private GroupDocumentsDao groupsDao;
	
	public GroupDocumentDto createGroup(String title) {
		String creator = contextHolder.getLoggedUserPhone();
		GroupDocumentDto group = new GroupDocumentDto();
		group.setTitle(title);
		group = Document.builder(group).setAdmin(creator).build();
		groupsDao.create(group);
		notifyUsers(group);
		return group;
	}

	private void notifyUsers(GroupDocumentDto createGroup) {
		Set<String> users = getGroupUsersForNotify(createGroup);
		notifyUsers(createGroup, users);
	}

	private void notifyUsers(GroupDocumentDto createGroup, Set<String> users) {
		for(String phone : users){
			sendUpsert(phone, createGroup);
		}
	}
	
	private Set<String> getGroupUsersForNotify(GroupDocumentDto group){
		Set<String> users = new HashSet<String>();
		users.addAll(group.getRoles().getAdmins());
		users.addAll(group.getRoles().getAuthors());
		users.addAll(group.getRoles().getUsers());
		users.remove(contextHolder.getLoggedUserPhone());
		return users;
	}

	@Authorize(type=Roles.ADMIN, parameter=0)
	public void removeGroup(String groupId) {
		GroupDocumentDto group = getGroupById(groupId);
		Set<String> groupUsersForNotify = getGroupUsersForNotify(group);
		for(String phone : groupUsersForNotify){
			sendDelete(phone, groupId);
		}
		groupsDao.delete(group);
	}

	public GroupDocumentDto getGroupById(String groupId, String... userId) {
		GroupDocumentDto group = getGroupIdInternal(groupId);
		return group;
	}
	
	
	private GroupDocumentDto getGroupIdInternal(String groupId){
		GroupDocumentDto group = groupsDao.getById(groupId);
		Assert.notNull(group, String.format("Group with id: %s not found", groupId));
		return group;
	}

	private String getLoggedPhone(String... userPhone) {
		return userPhone.length > 0 ? userPhone[0] : contextHolder.getLoggedUserPhone();
	}

	@Authorize(type=Roles.ADMIN, parameter=0)
	public void addUsers(String groupId, Set<String> phones) {
		Assert.notEmpty(phones, "At least one phone number should be sent");
		GroupDocumentDto group = getGroupById(groupId);
		Document builder = Document.builder(group);
		Set<String> tempPhones = new HashSet<String>(phones);
		for(String phone: tempPhones){
			builder.setUser(phone);
		}
		group = builder.build();
		groupsDao.update(group, contextHolder.getLoggedUserPhone());
		notifyUsers(group);
	}

	@Authorize(type=Roles.ADMIN, parameter=0)
	public void removeUser(String groupId, @Authorize(parameter=0) String phone) {
		Assert.notNull(phone, "At least one phone number should be sent");
		GroupDocumentDto group = getGroupIdInternal(groupId);
		Assert.isTrue(!group.getCreatedBy().equals(phone), "Can't remove group creator");
		Set<String> users = getGroupUsersForNotify(group);
		group = Document.builder(group).removeUser(phone).build();
		groupsDao.update(group, contextHolder.getLoggedUserPhone());
		notifyUsers(groupsDao.getById(groupId), users);
	}

	public List<GroupDocumentDto> getAllGroups(String... userPhone) {
		String currentUser = getLoggedPhone(userPhone);
		return groupsDao.getAll(currentUser);
	}

	@Authorize(type=Roles.ADMIN, parameter=0)
	public void updateGroup(GroupDocumentDto group) {
		GroupDocumentDto dbGroup = getGroupById(group.getId());
		Assert.notNull(dbGroup, "Group Not Found");
		try{
			group.setCreatedBy(null);
			group.setRoles(null);
			group.setTasks(null);
			group.setCreatedAt(dbGroup.getCreatedAt());
			Common.nullAwareBeanCopy(dbGroup, group);
			groupsDao.update(dbGroup, contextHolder.getLoggedUserPhone());
			notifyUsers(dbGroup);
		} catch(Exception e){
			throw new IllegalArgumentException("Can't update group");
		}
	}
	
	public void assignUserRole(String groupId, RoleDto roles){
		GroupDocumentDto group = groupsDao.getById(groupId);
		for(String userPhone : roles.getAdmins()){
			if (!userPhone.equals(group.getCreatedBy())){
				groupsDao.removeUserRole(groupId, userPhone);
				groupsDao.assignUserRole(Roles.ADMIN, userPhone, groupId);
			}
		}
		for(String userPhone : roles.getAuthors()){
			if (!userPhone.equals(group.getCreatedBy())){
				groupsDao.removeUserRole(groupId, userPhone);
				groupsDao.assignUserRole(Roles.AUTHOR, userPhone, groupId);
			}
		}
		for(String userPhone : roles.getUsers()){
			if (!userPhone.equals(group.getCreatedBy())){
				groupsDao.removeUserRole(groupId, userPhone);
				groupsDao.assignUserRole(Roles.USER, userPhone, groupId);
			}
		}
		group = groupsDao.getById(groupId);
		for(String phone : Document.builder(group).getGroupPhones(contextHolder.getLoggedUserPhone())){
			sendUpsert(phone, group, "roles");
		}
	}
}
