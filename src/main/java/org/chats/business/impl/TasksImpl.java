package org.chats.business.impl;

import java.util.List;
import java.util.Set;

import org.chats.authorization.Authorize;
import org.chats.authorization.ContextHolder;
import org.chats.business.Tasks;
import org.chats.dao.GroupDocumentsDao;
import org.chats.dao.UsersDao;
import org.chats.db.dto.GroupDocumentDto;
import org.chats.db.dto.TaskDto;
import org.chats.db.dto.TaskItemDto;
import org.chats.db.dto.UserDto;
import org.chats.documents.Document;
import org.chats.enums.Roles;
import org.chats.enums.TaskType;
import org.chats.enums.UserType;
import org.chats.event.MessageDecorator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

public class TasksImpl extends MessageDecorator implements Tasks {
	
	@Autowired
	private GroupDocumentsDao groupsDao;
	
	@Autowired
	private UsersDao usersDao;
	
	@Autowired
	ContextHolder contextHolder;

	@Authorize(type=Roles.ADMIN, parameter=0)
	public TaskDto createTask(String groupId, String taskName, TaskType type, String properties, List<String> answers, Set<TaskItemDto> items) {
		Assert.hasText(taskName, "Task name cannot be empty");
		GroupDocumentDto byId = groupsDao.getById(groupId);
		Assert.notNull(byId, "Group doesn't exists");
		Assert.notNull(type, "Task type can't be empty!");
		checkUserPermissions();
		TaskDto newTask = new TaskDto();
		newTask.setTaskName(taskName);
		newTask.setType(type);
		if (StringUtils.hasLength(properties)){
			newTask.setProperties(properties);
		}
		
		if (answers != null){
			newTask.setAnswers(answers);
		}
		
		if (items != null){
			newTask.setItems(items);
		}
		long currentTimeMillis = System.currentTimeMillis();
		newTask.setCreatedAt(currentTimeMillis);
		String loggedUserPhone = contextHolder.getLoggedUserPhone();
		newTask.setCreatedBy(loggedUserPhone);
		newTask.setUpdatedAt(currentTimeMillis);
		Document addTask = Document.builder(byId);
		groupsDao.pushTask(newTask, groupId);
		for(String phone : addTask.getGroupPhones(loggedUserPhone)){
			sendUpsert(phone, new TaskResponse(groupId, newTask), "task");
		}
		return newTask;
	}

	@Authorize(type=Roles.ADMIN, parameter=0)
	public void removeTask(String groupId, String taskId) {
		GroupDocumentDto group = groupsDao.getById(groupId);
		Assert.notNull(group, "Group Not Found");
		Document removeTask = Document.builder(group);
		Assert.notNull(removeTask, "Can't Add Task Item");
		String loggedUserPhone = contextHolder.getLoggedUserPhone();
		for(String phone : removeTask.getGroupPhones(loggedUserPhone)){
			sendDelete(phone, groupId + "/" +taskId, "task");
		}
		groupsDao.pullTask(taskId);
	}
	
	@Authorize(type=Roles.USER, parameter=0)
	public GroupDocumentDto addTaskItem(String groupId, String taskId, String itemTitle, String userId, String answer){
		GroupDocumentDto group = groupsDao.getById(groupId);
		Assert.notNull(group, "Group Not Found");
		Assert.hasText(itemTitle, "Title cannot be empty");
		Document builder = Document.builder(group);
		String loggedUserPhone = contextHolder.getLoggedUserPhone();
		TaskItemDto taskItem = new TaskItemDto();
		taskItem.setTitle(itemTitle);
		taskItem.setUpdateAt(System.currentTimeMillis());
		taskItem.setUserId(userId);
		taskItem.setAnswer(answer);
		builder = builder.addTaskItem(taskId, taskItem);
		Assert.notNull(builder, "Can't Add Task Item");
		for(String phone : builder.getGroupPhones(loggedUserPhone)){
			sendUpsert(phone, new TaskResponse(groupId, builder.getTaskById(taskId)), "item");
		}
		groupsDao.update(group, loggedUserPhone);
		return group;
	}
	
	@Authorize(type=Roles.ADMIN, parameter=0)
	public GroupDocumentDto removeTaskItem(String groupId, String taskId, String itemId){
		GroupDocumentDto group = groupsDao.getById(groupId);
		Assert.notNull(group, "Group Not Found");
		Document groupDoc = Document.builder(group).removeTaskItem(taskId, itemId);
		Assert.notNull(groupDoc, "Task/Item not Found");
		String loggedUserPhone = contextHolder.getLoggedUserPhone();
		for(String phone : groupDoc.getGroupPhones(loggedUserPhone)){
			sendDelete(phone, new TaskResponse(groupId, groupDoc.getTaskById(taskId)), "item");
		}
		groupsDao.update(group, loggedUserPhone);
		return group;
	}

	@Authorize(type=Roles.ADMIN, parameter=1)
	public void modifyTask(TaskDto taskDto, String groupId) {
		GroupDocumentDto group = groupsDao.getById(groupId);
		Assert.notNull(group, "Group Not Found");
		Document modifyTask = Document.builder(group).modifyTask(taskDto);
		Assert.notNull(modifyTask, "Task/Item not Found");
		String loggedUserPhone = contextHolder.getLoggedUserPhone();
		for(String phone : modifyTask.getGroupPhones(loggedUserPhone)){
			sendUpsert(phone, new TaskResponse(groupId, taskDto), "task");
		}
		groupsDao.update(group, loggedUserPhone);
	}

	@Authorize(type=Roles.USER, parameter=0)
	public GroupDocumentDto changeTaskItemState(String groupId, String taskId, String itemId, String answer){
		GroupDocumentDto group = groupsDao.getById(groupId);
		Assert.notNull(group, "Group Not Found");
		String loggedUserPhone = contextHolder.getLoggedUserPhone();
		Document setItemAnswer = Document.builder(group).setItemAnswer(taskId, itemId, answer, loggedUserPhone);
		Assert.notNull(setItemAnswer, "Task/Item not Found");
		for(String phone : setItemAnswer.getGroupPhones(loggedUserPhone)){
			sendUpsert(phone, new TaskResponse(groupId, setItemAnswer.getTaskById(taskId)), "item");
		}
		groupsDao.update(group, loggedUserPhone);
		return group;
	}
	
	private void checkUserPermissions(){
		String userPhone = contextHolder.getLoggedUserPhone();
		UserDto user = usersDao.getUserByPhone(userPhone);
		if (user.getUserType().equals(UserType.REGULAR)){
			List<GroupDocumentDto> actualGroups = groupsDao.getAll(userPhone);
			for(GroupDocumentDto group : actualGroups){
				List<TaskDto> tasks = group.getTasks();
				for(TaskDto task : tasks){
					if (task.getCreatedBy().equals(user.getPhone())){
						throw new IllegalArgumentException("Your aren't permitted to create more than 1 Task");
					}	
				}
			}
		}
	}
	
	private class TaskResponse{
		
		private String groupId;
		
		private TaskDto task;

		public TaskResponse(String groupId, TaskDto task){
			this.groupId = groupId;
			this.task = task;
		}
		
		@SuppressWarnings("unused")
		public String getGroupId() {
			return groupId;
		}

		@SuppressWarnings("unused")
		public void setGroupId(String groupId) {
			this.groupId = groupId;
		}

		@SuppressWarnings("unused")
		public TaskDto getTask() {
			return task;
		}

		@SuppressWarnings("unused")
		public void setTask(TaskDto task) {
			this.task = task;
		}
	}
}
