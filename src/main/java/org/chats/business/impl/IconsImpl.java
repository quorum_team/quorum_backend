package org.chats.business.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.chats.business.Icons;
import org.chats.enums.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;

import sun.misc.BASE64Encoder;

import com.dropbox.core.DbxException;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.DbxFiles.FileMetadata;
import com.dropbox.core.v2.DbxFiles.GetMetadataException;
import com.dropbox.core.v2.DbxFiles.Metadata;

@SuppressWarnings("restriction")
public class IconsImpl implements Icons {
	
	private final static String ICONS_PATH = "/intheknow/icons";
	private final static String PUBLIC_FOLDER = ICONS_PATH + "/public";
	private final static String PREMIUM_FOLDER = ICONS_PATH + "/premium";
	
	@Autowired
	private DbxClientV2 client;
	
	public Map<UserType, List<String>> iconsList() throws DbxException {
		Map<UserType, List<String>> files = new HashMap<UserType, List<String>>();
		files.put( UserType.REGULAR, new ArrayList<String>());
		files.put( UserType.PREMIUM, new ArrayList<String>());
		populateFolder(files.get(UserType.REGULAR), PUBLIC_FOLDER);
		populateFolder(files.get(UserType.PREMIUM), PREMIUM_FOLDER);
		return files;
	}
	
	private void populateFolder(List<String> files, String folder) throws DbxException{
		ArrayList<Metadata> entries = client.files.listFolder(folder).entries;
		for (Metadata metadata : entries) {
			files.add(metadata.name);
		}
	}
	
	public void downloadPublicFile(String iconPath, HttpServletResponse response){
		downloadFile(PUBLIC_FOLDER, iconPath, response);
	}
	
	public void downloadPremiumFile(String iconPath, HttpServletResponse response){
		downloadFile(PREMIUM_FOLDER, iconPath, response);
	}

	private void downloadFile(String fullPath, String iconPath, HttpServletResponse response) {
		try{
			String path = fullPath + "/" + iconPath;
			FileMetadata metadata = (FileMetadata)client.files.getMetadata(path);
			ServletOutputStream outputStream = response.getOutputStream();
			String fileName = metadata.name;
			response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", fileName));
			iconPath = iconPath.toLowerCase();
			String[] split = iconPath.split("\\.");
			response.setContentType(String.format("image/%s", split[split.length - 1]));
			response.setHeader("content-length", String.valueOf((metadata).size));
			populateOutPutStream(path, outputStream);
			outputStream.flush();
			outputStream.close();
		} catch(Exception e){
			throw new IllegalArgumentException("Can't find image");
		}
	}
	
	@Cacheable(value="iconsMarket", key="#iconPath")
	public String getPublicFile(String iconPath) throws GetMetadataException, DbxException, IOException{
		return getFile(getPath(PUBLIC_FOLDER, iconPath));
	}

	private String getFile(String path) throws DbxException, IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		populateOutPutStream(path, bos);
		byte[] imageBytes = bos.toByteArray();
		BASE64Encoder encoder = new BASE64Encoder(); //DatatypeConverter
		bos.flush();
		bos.close();
        return encoder.encode(imageBytes);
	}
	
	@Cacheable(value="iconsMarket", key="#iconPath")
	public String getPremiumFile(String iconPath) throws GetMetadataException, DbxException, IOException{
		return getFile(getPath(PREMIUM_FOLDER, iconPath));
	}
	
	private void populateOutPutStream(String path, OutputStream ous) throws DbxException, IOException{
		client.files.downloadBuilder(path).run(ous);
	}
	
	private String getPath(String folder, String filename){
		return String.format("%s/%s", folder, filename);
	}
}
