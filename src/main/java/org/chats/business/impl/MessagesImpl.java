package org.chats.business.impl;

import java.util.ArrayList;
import java.util.List;

import org.chats.authorization.Authorize;
import org.chats.business.Messages;
import org.chats.dao.GroupDocumentsDao;
import org.chats.dao.MessagesDao;
import org.chats.db.dto.GroupDocumentDto;
import org.chats.db.dto.MessageContentDto;
import org.chats.documents.Document;
import org.chats.enums.Roles;
import org.chats.event.MessageDecorator;
import org.chats.stomp.Message;
import org.springframework.beans.factory.annotation.Autowired;

public class MessagesImpl extends MessageDecorator implements Messages {
	
	@Autowired
	private GroupDocumentsDao groupsDao;
	
	@Autowired
	private MessagesDao messagesDao;
	
	@Authorize(type=Roles.AUTHOR, parameter=2)
	public MessageContentDto pushMessage(MessageContentDto message, @Authorize String userId, String groupId) {
		GroupDocumentDto byId = groupsDao.getById(message.getGroupId());
		message.setUsers(Document.builder(byId).getGroupPhones(userId));
		message.setCreateAt(System.currentTimeMillis());
		message.setCreateBy(userId);
		messagesDao.upsert(message);
		return message;	
	}
	
	public void pullMessage(MessageContentDto messageContentDto){
		if (messageContentDto != null){
			for(String phone : messageContentDto.getUsers()){
				sendDraft(phone, new Message[]{convertToMessage(messageContentDto)});
			}	
		}
	}
	
	private Message convertToMessage(MessageContentDto messageContent){
		Message messageToSend = new Message();
		messageToSend.setContent(new String(messageContent.getContent()));
		messageToSend.setCreatedAt(messageContent.getCreateAt());
		messageToSend.setGroupId(messageContent.getGroupId());
		messageToSend.setType(messageContent.getType());
		messageToSend.setCreateBy(messageContent.getCreateBy());
		messageToSend.setMessageId(messageContent.getId());
		return messageToSend;
	}
	
	public void pullMessage(String phone){
		List<Message> messages = new ArrayList<Message>();
		for(MessageContentDto message: messagesDao.getMessagesByPhone(phone)){
			messages.add(convertToMessage(message));
		}
		sendDraft(phone, messages);
	}

	public void removeMessage(String messageId, String userPhone){
		messagesDao.pullUser(messageId, userPhone);
		messagesDao.removeOrphanMessages();
	}
}
