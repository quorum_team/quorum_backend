package org.chats.servlet;

import static org.chats.utils.Common.isNull;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;

import org.chats.authorization.ContextHolder;
import org.chats.business.Users;
import org.chats.dao.RegistrationDao;
import org.chats.db.dto.RegisterDto;
import org.chats.db.dto.UserDto;
import org.chats.plivo.PlivoService;
import org.chats.rest.AbstractController;
import org.chats.utils.CodeGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.plivo.helper.exception.PlivoException;

@Controller
public class LoginServlet extends AbstractController {

	@Autowired
	Users usersBusinessLayer;

	@Autowired
	ContextHolder contextHolder;

	@Autowired
	CodeGenerator codeGenerator;
	
	@Autowired
	private PlivoService plivoSerive;
	
	@Autowired
	private RegistrationDao registrationDao; 

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String registerUser(@RequestBody UserDto userReg,
			Model model, Device device, HttpServletRequest request) throws IllegalArgumentException, PlivoException {
		UserDto user = new UserDto();
		user.setPhone(userReg.getPhone());
		user.setCountryExt(userReg.getCountryExt());
		user.setCode(codeGenerator.getCode());
		user.setAltPhone(userReg.getCountryExt() + userReg.getPhone().replaceFirst(userReg.getCountryExt(), "0"));
		user.setDevice(device.getDevicePlatform());
		user = usersBusinessLayer.addUser(user);
		RegisterDto byId = registrationDao.getById(user.getPhone());
		if (byId == null || byId.getDates().size() < 4) {
			if (registrationDao.getDatesCount() <= 2000){
				plivoSerive.sendCode(user.getPhone(), user.getCode());	
			}
		}
		registrationDao.addEntry(isNull(request.getHeader("X-FORWARDED-FOR"), request.getRemoteAddr()), user.getPhone(), System.currentTimeMillis());
		return responseOk(model);
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String login(Model model) throws AuthenticationException {
		contextHolder.removeUserId();
		return responseOk(model);
	}
}
