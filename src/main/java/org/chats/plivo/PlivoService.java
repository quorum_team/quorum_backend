package org.chats.plivo;

import java.util.LinkedHashMap;

import com.plivo.helper.api.client.RestAPI;
import com.plivo.helper.exception.PlivoException;

public class PlivoService {

	private String authId;
	private String authToken;
	
	public void setAuthId(String authId) {
		this.authId = authId;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
	
	public void sendCode(String phone, String code) throws PlivoException{
		try{
			RestAPI api = new RestAPI(authId, authToken, "v1");
			LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
	        parameters.put("src", "AppDatez");
	        parameters.put("dst", phone);
	        parameters.put("text", "Appdatez account confirmation code: " + code);
	        
	        api.sendMessage(parameters);
		} catch (Exception e){
			// Nothing to do meanwhile...
		}

 	}
}