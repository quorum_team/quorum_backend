package org.chats.utils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.beanutils.BeanUtilsBean;

public class Common {
	
	public static void nullAwareBeanCopy(Object dest, Object source) throws IllegalAccessException, InvocationTargetException {
	    new BeanUtilsBean() {
	        @Override
	        public void copyProperty(Object dest, String name, Object value)
	                throws IllegalAccessException, InvocationTargetException {
	            if(value != null) {
	                super.copyProperty(dest, name, value);
	            }
	        }
	    }.copyProperties(dest, source);
	}
	
	public static String isNull(String str, String defaultValue){
		if (str == null){
			return defaultValue;
		}
		return str;
	}
	
	public static long calculateMonthOldData(){
		Calendar calendar = new GregorianCalendar();
		if (calendar.get(Calendar.MONTH) == Calendar.JANUARY){
			calendar.set(Calendar.MONTH, Calendar.DECEMBER);
			calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 1);
		} else {
			calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
		}
		return calendar.getTimeInMillis();
	}
	
	public static List<String> getNormalizedPhonesList(String countryCode, List<String> phones){
		List<String> nPhones = new ArrayList<String>(phones.size());
		for(String phone : phones){
			nPhones.add(getNormalizedPhone(countryCode, phone));
		}
		return nPhones;
	}
	
	public static String getNormalizedPhone(String countryCode, String phone){
		phone = String.valueOf(Long.parseLong(phone.replaceAll("[^\\d.]", "")));
		if (phone.length() < 10){
			phone = countryCode + phone;
		}
		return phone;
	}
}
