package org.chats.utils;

import java.util.Random;

public class CodeGenerator {
	
	public String getCode(){
		Random r = new Random();
		int code = r.nextInt(99900) + 100;
		return getCodeString(code);
	}
	
	private String getCodeString(int code){
		String tempStr = "00" + code;
		return "" + tempStr.charAt(tempStr.length() - 5) +  
			   tempStr.charAt(tempStr.length() - 4) +
			   tempStr.charAt(tempStr.length() - 3) +
			   tempStr.charAt(tempStr.length() - 2) +
			   tempStr.charAt(tempStr.length() - 1);
	}

}
