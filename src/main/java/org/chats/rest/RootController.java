package org.chats.rest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value="/", produces="application/json")
public class RootController extends AbstractController {

    @RequestMapping(method=RequestMethod.GET )
    public String root(Model model)
    {
    	return responseOk(model);
    }
}
