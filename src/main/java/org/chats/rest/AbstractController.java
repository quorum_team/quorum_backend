package org.chats.rest;

import org.springframework.ui.Model;

public class AbstractController {

	protected String responseOk(Object dto, Model model){
		model.addAttribute(getCollectionName(), dto);
		model.addAttribute("status", "ok");
		return "jsonTemplate";
	}
	
	protected String responseOk(Model model){
		model.addAttribute("status", "ok");
		return "jsonTemplate";
	}
	
	private String getCollectionName(){
		String[] classParts = this.getClass().getName().split("[.]");
		return classParts[classParts.length - 1].replace("Controller", "").toLowerCase();
	}
}
