package org.chats.rest;

import static org.chats.utils.Common.isNull;

import org.chats.business.Groups;
import org.chats.business.Tasks;
import org.chats.db.dto.GroupDocumentDto;
import org.chats.db.dto.TaskDto;
import org.chats.db.dto.TaskItemDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value="/tasks", produces="application/json")
public class TasksController extends AbstractController{
	
	@Autowired
	private Tasks tasksBusinessLayer;
	
	@Autowired
	private Groups groupsBusinessLayer;

	@RequestMapping(value = "/{groupId}",method=RequestMethod.POST, consumes="application/json")
	public String createNewTask(@PathVariable String groupId, @RequestBody TaskDto taskDto, Model model){
		TaskDto createTask = tasksBusinessLayer.createTask(groupId, taskDto.getTaskName(), taskDto.getType(), taskDto.getProperties(), taskDto.getAnswers(), taskDto.getItems());
		return responseOk(createTask, model);
	}
	
	@RequestMapping(value = "/{groupId}/{taskId}", method=RequestMethod.DELETE)
	public String deleteTask(@PathVariable String taskId,@PathVariable String groupId, Model model){
		tasksBusinessLayer.removeTask(groupId, taskId);
		return responseOk(model);   	
	}
	
	@RequestMapping(value = "/{groupId}", method=RequestMethod.PUT, consumes="application/json")
	public String modifyTask(@RequestBody TaskDto taskDto, @PathVariable String groupId, Model model){
		tasksBusinessLayer.modifyTask(taskDto, groupId);
		return responseOk(model);   	
	}
	
	@RequestMapping(value = "/{groupId}",method=RequestMethod.GET)
	public String getGroupTasks(@PathVariable String groupId, Model model){
		return responseOk(groupsBusinessLayer.getGroupById(groupId).getTasks(), model);   	
	}
	
	@RequestMapping(value = "/{groupId}/{taskId}",method=RequestMethod.POST)
	public String addTaskItem(@PathVariable String groupId, @PathVariable String taskId, @RequestBody TaskItemDto taskItemDto, Model model){
		GroupDocumentDto task = tasksBusinessLayer.addTaskItem(groupId, taskId, taskItemDto.getTitle(), taskItemDto.getUserId(), isNull(taskItemDto.getAnswer(), ""));
		return responseOk(task, model);   	
	}
	
	@RequestMapping(value = "/{groupId}/{taskId}/{itemId}",method=RequestMethod.DELETE)
	public String removeTaskItem(@PathVariable String groupId, @PathVariable String taskId, @PathVariable String itemId, Model model){
		GroupDocumentDto task = tasksBusinessLayer.removeTaskItem(groupId, taskId, itemId);
		return responseOk(task, model);   	
	}
	
	@RequestMapping(value = "/{groupId}/{taskId}/{itemId}/{answer}",method=RequestMethod.PUT)
	public String changeItemState(@PathVariable String groupId, @PathVariable String taskId,
			@PathVariable String itemId, @PathVariable String answer, Model model){
		GroupDocumentDto task = tasksBusinessLayer.changeTaskItemState(groupId, taskId, itemId, answer);
		return responseOk(task, model);   	
	}
	
	@RequestMapping(value = "/{groupId}/{taskId}/{itemId}",method=RequestMethod.PUT)
	public String changeItemStateEmpty(@PathVariable String groupId, @PathVariable String taskId,
			@PathVariable String itemId, Model model){
		GroupDocumentDto task = tasksBusinessLayer.changeTaskItemState(groupId, taskId, itemId, "");
		return responseOk(task, model);   	
	}
}
