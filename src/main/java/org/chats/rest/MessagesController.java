package org.chats.rest;

import java.util.Iterator;

import org.chats.business.Messages;
import org.chats.dao.UsersDao;
import org.chats.db.dto.MessageContentDto;
import org.chats.db.dto.UserDto;
import org.chats.enums.MessageType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


@Controller
public class MessagesController {

	@Autowired
	private Messages messagesBusinessLayer;
	
	@Autowired
	private UsersDao usersDao;
	
	@MessageMapping("/pullmessages")
    public void pullmessages(SimpMessageHeaderAccessor headerAccessor) throws Exception {
		UserDto currentUser = usersDao.getUserByPhone(headerAccessor.getUser().getName());
		Assert.notNull(currentUser, "User doesn't exists");
    	messagesBusinessLayer.pullMessage(currentUser.getPhone());
    }
    
    @MessageMapping("/message")
    public void message(Object message, SimpMessageHeaderAccessor headerAccessor) throws Exception {
    	MessageContentDto messageContentDto = convertToMessage(message);
    	String currentPhone = headerAccessor.getUser().getName();
    	Assert.notNull(currentPhone, "User doesn't exists");
		MessageContentDto pushMessage = messagesBusinessLayer.pushMessage(messageContentDto, currentPhone, messageContentDto.getGroupId());
    	messagesBusinessLayer.pullMessage(pushMessage);
    }
    
    private MessageContentDto convertToMessage(Object messageObj){
    	JsonParser jsonParser = new JsonParser();
    	JsonObject messageJson = jsonParser.parse(new String((byte[])messageObj)).getAsJsonObject();
    	String groupId = messageJson.get("groupId").getAsString();
    	String content = messageJson.get("content").getAsString();
    	MessageType type = MessageType.getEnumValue((messageJson.get("type").getAsString()));
    	MessageContentDto messageContentDto = new MessageContentDto();
    	messageContentDto.setContent(content);
    	messageContentDto.setGroupId(groupId);
    	messageContentDto.setType(type);
    	return messageContentDto;
    }
    
    @MessageMapping("/received")
    public void messageReceived(Object message, SimpMessageHeaderAccessor headerAccessor){
    	JsonParser jsonParser = new JsonParser();
    	JsonObject idsJson = jsonParser.parse(new String((byte[])message)).getAsJsonObject();
    	Iterator<JsonElement> iterator = idsJson.getAsJsonArray("ids").iterator();
    	String currentPhone = headerAccessor.getUser().getName();
    	while(iterator.hasNext()){
    		String messageId = iterator.next().getAsString();
    		messagesBusinessLayer.removeMessage(messageId, currentPhone);
    	}
    }
}