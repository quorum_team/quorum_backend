package org.chats.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.chats.business.Users;
import org.chats.db.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value="/users", produces="application/json")
public class UsersController extends AbstractController {
	
	@Autowired
	Users usersBusinessLayer;
    
    @RequestMapping(method=RequestMethod.GET )
    public String getAllUsers(Model model, HttpServletRequest request)
    {
    	return responseOk(usersBusinessLayer.getAllUsers(), model);
    }
    
    @RequestMapping(value = "/registered", method=RequestMethod.POST, consumes="application/json")
    public String getRegisteredUsers(@RequestBody List<String> phones, Model model)
    {
  	   	return responseOk(usersBusinessLayer.getRegisteredUsers(phones), model);
    }
    
    @RequestMapping(value = "/user", method=RequestMethod.PUT, consumes="application/json")
    public String updateUser(@RequestBody UserDto user, Model model){
    	usersBusinessLayer.updateUser(user);
    	return responseOk(model);
    }
    
    @RequestMapping(value = "/info", method=RequestMethod.GET)
    public String getUserInfo(Model model){
    	model.addAttribute("info", usersBusinessLayer.getCurrentUserInfo());
    	return responseOk(model);
    }
}
