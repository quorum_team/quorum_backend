package org.chats.rest;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.chats.business.Icons;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.dropbox.core.DbxException;

@Controller
@RequestMapping(value="/market")
public class IconsController extends AbstractController{

	@Autowired
	private Icons iconsBusinessLayer;

	@RequestMapping(produces="application/json", method = RequestMethod.GET)
	public String iconsList(Model model) throws IllegalArgumentException, DbxException, IOException {
		return responseOk(iconsBusinessLayer.iconsList(), model);
	}
	
	@RequestMapping(value = "public/{fileName}/download", method = RequestMethod.GET)
	public void downloadPublicFile(@PathVariable String fileName, HttpServletResponse response) throws DbxException, IOException {
		iconsBusinessLayer.downloadPublicFile(fileName, response);
	}
	
	@RequestMapping(value = "premium/{fileName}/download", method = RequestMethod.GET)
	public void downloadPremiumFile(@PathVariable String fileName, HttpServletResponse response) throws DbxException, IOException {
		iconsBusinessLayer.downloadPremiumFile(fileName, response);
	}
	
	@RequestMapping(value = "public/{fileName}/encode", method = RequestMethod.GET, produces="application/json")
	public String getPublicFile(@PathVariable String fileName, Model model) throws DbxException, IOException {
		return responseOk(iconsBusinessLayer.getPublicFile(fileName), model);
	}
	
	@RequestMapping(value = "premium/{fileName}/encode", method = RequestMethod.GET, produces="application/json")
	public String getPremiumFile(@PathVariable String fileName, Model model) throws DbxException, IOException {
		return responseOk(iconsBusinessLayer.getPremiumFile(fileName), model);
	}
}
