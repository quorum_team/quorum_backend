package org.chats.rest;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.chats.business.Groups;
import org.chats.db.dto.GroupDocumentDto;
import org.chats.db.dto.RoleDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value="/groups", produces="application/json")
public class GroupsController extends AbstractController {
	
	@Autowired
	private Groups groupBusinessLayer;
	
	@RequestMapping(value = "/group", method=RequestMethod.POST, consumes="application/json")
    public String createGroup(@RequestBody GroupDocumentDto group, Model model) throws IllegalArgumentException
    {
    	GroupDocumentDto createdGroup = groupBusinessLayer.createGroup(group.getTitle());
    	return responseOk(createdGroup, model);   		
    }
    
    @RequestMapping(value = "/{groupId}", method=RequestMethod.DELETE)
    public String removeGroup(@PathVariable String groupId, Model model)
    {
    	groupBusinessLayer.removeGroup(groupId);
    	return responseOk(model);   		
    }
    
    @RequestMapping(method=RequestMethod.PUT)
    public String updateGroup(@RequestBody GroupDocumentDto group, Model model)
    {
    	groupBusinessLayer.updateGroup(group);
    	return responseOk(model);   		
    }
    
    @RequestMapping(value = "/users/{groupId}", method=RequestMethod.POST, consumes="application/json")
    public String addUsers(@RequestBody Set<String> phones, @PathVariable String groupId, Model model) throws IllegalArgumentException
    {
    	groupBusinessLayer.addUsers(groupId, phones);
    	return responseOk( model);   		
    }
    
    @RequestMapping(value = "/users/{groupId}/{phone}", method=RequestMethod.DELETE)
    public String removeUser(@PathVariable String groupId, @PathVariable String phone, Model model) throws IllegalArgumentException
    {
    	groupBusinessLayer.removeUser(groupId, phone);
    	return responseOk( model);   		
    }
    
    @RequestMapping(method=RequestMethod.GET )
    public String getMyGroups(Model model)
    {
    	List<GroupDocumentDto> allGroups = groupBusinessLayer.getAllGroups();
    	return responseOk(allGroups, model);
    }
    
    @RequestMapping(value = "/users/{groupId}", method=RequestMethod.PUT, consumes="application/json")
    public String assignUsersRoles(@RequestBody RoleDto roles, @PathVariable String groupId, Model model) throws IllegalArgumentException
    {
    	if (roles.getAdmins() == null){
    		roles.setAdmins(new HashSet<String>());
    	}
    	if (roles.getAuthors() == null){
    		roles.setAuthors(new HashSet<String>());
    	}
    	if (roles.getUsers() == null){
    		roles.setUsers(new HashSet<String>());
    	}
    	
    	groupBusinessLayer.assignUserRole(groupId, roles);
    	return responseOk( model);   		
    }
}
