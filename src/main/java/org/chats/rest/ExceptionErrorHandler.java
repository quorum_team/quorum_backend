package org.chats.rest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class ExceptionErrorHandler {
	
	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ModelAndView exception(Exception e) {
        ModelAndView mav = new ModelAndView("jsonTemplate");
        mav.addObject("message", e.getMessage());
        mav.addObject("status", "error");
        return mav;
    }
	
	@ExceptionHandler(SecurityException.class)
	@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    public ModelAndView unauthorizedException(Exception e) {
        ModelAndView mav = new ModelAndView("jsonTemplate");
        mav.addObject("message", e.getMessage());
        mav.addObject("status", "error");
        return mav;
    }
	
	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.I_AM_A_TEAPOT)
    public ModelAndView genralException(Exception e) {
        ModelAndView mav = new ModelAndView("jsonTemplate");
        mav.addObject("message", e.getMessage());
        mav.addObject("status", "error");
        return mav;
    }
}
