package org.chats.documents;

import java.util.HashSet;
import java.util.Set;

import org.chats.db.dto.GroupDocumentDto;
import org.chats.db.dto.RoleDto;
import org.chats.db.dto.TaskDto;
import org.chats.db.dto.TaskItemDto;
import org.chats.utils.Common;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

public class Document {

	private GroupDocumentDto group;
	private RoleDto roles;

	private Document(GroupDocumentDto group){
		Assert.notNull(group, "Group Cannot be null");
		this.group = group;
		this.roles = group.getRoles();
	}
	
	public static Document builder(GroupDocumentDto group){
		Document instance = new Document(group);
		return instance;
	}
	
	public Document setAdmin(String phone){
		roles.getAdmins().add(phone);
		roles.getAuthors().remove(phone);
		roles.getUsers().remove(phone);
		return this;
	}
	
	public Document setUser(String phone){
		roles.getAdmins().remove(phone);
		roles.getAuthors().remove(phone);
		roles.getUsers().add(phone);
		return this;
	}
	
	public Document setAuthor(String phone){
		roles.getAdmins().remove(phone);
		roles.getAuthors().add(phone);
		roles.getUsers().remove(phone);
		return this;
	}
	
	public GroupDocumentDto build(){
		return group;
	}
	
	public Document removeUser(String phone){
		roles.getAdmins().remove(phone);
		roles.getAuthors().remove(phone);
		roles.getUsers().remove(phone);
		return this;
	}
	
	public Set<String> getGroupPhones(String... excludedPhones){
		Set<String> phones = new HashSet<String>();
		phones.addAll(group.getRoles().getAdmins());
		phones.addAll(group.getRoles().getUsers());
		phones.addAll(group.getRoles().getAuthors());
		for(String excludedPhone : excludedPhones){
			phones.remove(excludedPhone);
		}
		return phones;
	}
	
	public Document addTask(TaskDto task){
		group.getTasks().add(task);
		return this;
	}
	
	public Document removeTask(String taskId){
		for(TaskDto task : group.getTasks()){
			if (task.getId().equals(taskId)){
				group.getTasks().remove(task);
				return this;
			}
		}
		return null;
	}
	
	public boolean isAdmin(String userPhone){
		return group.getRoles().getAdmins().contains(userPhone);
	}
	
	public boolean isAuthor(String userPhone){
		return group.getRoles().getAuthors().contains(userPhone);
	}
	
	public boolean isUser(String userPhone){
		return group.getRoles().getUsers().contains(userPhone);
	}
	
	public Document addTaskItem(String taskId, TaskItemDto item){
		for(TaskDto task : group.getTasks()){
			if (task.getId().equals(taskId)){
				if (task.getAnswers().contains(item.getAnswer()) || !StringUtils.hasText(item.getAnswer())){
					if (getGroupPhones().contains(item.getUserId()) || !StringUtils.hasText(item.getUserId())){
						task.getItems().add(item);
						return this;						
					}
				}
			}
		}
		return null;
	}
	
	public Document removeTaskItem(String taskId, String itemId){
		for(TaskDto task : group.getTasks()){
			if (task.getId().equals(taskId)){
				for(TaskItemDto item : task.getItems()){
					if (item.getId().equals(itemId)){
						task.getItems().remove(item);
						return this;
					}
				}
			}
		}
		return null;
	}
	
	public TaskDto getTaskById(String taskId){
		for(TaskDto task : group.getTasks()){
			if (task.getId().equals(taskId)){
				return task;
			}
		}
		return null;
	}
	
	public Document modifyTask(TaskDto mtask){
		try{
			for(TaskDto task : group.getTasks()){
				if (task.getId().equals(mtask.getId())){
					Common.nullAwareBeanCopy(task, mtask);
					return this;
				}
			}			
		} catch(Exception e){
			return null;	
		}
		return null;
	}
	
	public Document setItemAnswer(String taskId, String itemId, String answer, String userPhone){
		for(TaskDto task : group.getTasks()){
			if (task.getId().equals(taskId)){
				if (task.getAnswers().contains(answer) || !StringUtils.hasText(answer)){
					for(TaskItemDto item : task.getItems()){
						if (item.getId().equals(itemId) && item.getUserId().equals(userPhone)){
							item.setAnswer(answer);
							return this;
						}
					}
				}
			}
		}
		return null;
	}
}
