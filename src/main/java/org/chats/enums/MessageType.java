package org.chats.enums;

public enum MessageType {

	TEXT("TEXT"),
	IMAGE("IMAGE");

	private String value;

	private MessageType(String value) {
		this.setValue(value);
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public static MessageType getEnumValue(String value){
		for(MessageType type: values()){
			if (type.getValue().equals(value)){
				return type;
			}
		}
		throw new IllegalArgumentException("No Such Message Type");
	}
}
