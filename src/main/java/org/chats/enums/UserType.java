package org.chats.enums;

public enum UserType {

	REGULAR(0),
	PREMIUM(1);
	
	private int userType = 0;
	
	private UserType(int userType){
		this.setUserType(userType);
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}
}
