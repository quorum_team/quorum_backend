package org.chats.enums;

public enum Roles {
	ADMIN,
	AUTHOR,
	USER;
}
