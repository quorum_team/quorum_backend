package org.chats.enums;

public enum TaskType {

	SURVEY(0),
	CHECKLIST(1),
	ATTENDING(2);
	
	private int val;
	
	private TaskType(int val){
		this.val = val;
	}
	
	public int getValue(){
		return this.val;
	}
}
