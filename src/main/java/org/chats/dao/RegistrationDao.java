package org.chats.dao;

import static org.springframework.data.mongodb.core.query.Criteria.where;

import java.util.HashSet;

import org.chats.db.dto.RegisterDto;
import org.chats.db.dto.ResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.mapreduce.MapReduceResults;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class RegistrationDao {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	public void addEntry(String ip, String phoneNumber, long timestamp){
		mongoTemplate.upsert(new Query(where("targetPhone").is(phoneNumber)),
				new Update().addToSet("ips", ip).addToSet("dates", timestamp),
				RegisterDto.class);
	}
	
	public RegisterDto getById(String phoneNmber){
		return mongoTemplate.findById(phoneNmber, RegisterDto.class);
	}
	
	public void deleteEntry(RegisterDto reg){
		mongoTemplate.remove(reg);
	}
	
	public void cleanDates(){
		mongoTemplate.updateMulti(null , new Update().set("dates", new HashSet<Long>()), RegisterDto.class);
	}
	
	public long getDatesCount(){
		try{
			MapReduceResults<ResultDto> mapReduce = mongoTemplate.mapReduce("registration", "function() { emit('dates', { count: this.dates.length }); }", "function(key, values) { var result = { count: 0 }; values.forEach(function(value) { result.count += value.count; }); return result; }", ResultDto.class);
			JsonParser parser = new JsonParser();
			return ((JsonObject)parser.parse(((ResultDto)mapReduce.iterator().next()).getValue())).get("count").getAsLong();
		}
		catch(Exception e){
			return 0;	
		}
	}
}
