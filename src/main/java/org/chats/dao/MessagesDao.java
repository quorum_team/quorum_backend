package org.chats.dao;

import static org.springframework.data.mongodb.core.query.Criteria.where;

import java.util.List;

import org.chats.db.dto.MessageContentDto;
import org.chats.utils.Common;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

public class MessagesDao {

	@Autowired
	MongoTemplate mongoTemplate;
	
	public void upsert(MessageContentDto message){
		mongoTemplate.save(message);
	}
	
	public void remove(String id){
		mongoTemplate.remove(new Query(where("id").is(id)), MessageContentDto.class);
	}
	
	public List<MessageContentDto> getMessagesByPhone(String userPhone){
		return mongoTemplate.find(new Query(where("users").in(new Object[] {userPhone})).with(new Sort(Sort.Direction.ASC, "createAt")), MessageContentDto.class);
	}
	
	public void pullUser(String id, String userPhone){
		mongoTemplate.updateFirst(new Query(where("id").is(id)),
				new Update().pull("users", userPhone),
				MessageContentDto.class);
	}
	
	public void removeOrphanMessages(){
		mongoTemplate.remove(new Query(where("users").size(0)), MessageContentDto.class);
	}
	
	public void removeMonthOldMessages(){
		mongoTemplate.remove(new Query(where("createAt").lte(Common.calculateMonthOldData())), MessageContentDto.class);
	}
}
