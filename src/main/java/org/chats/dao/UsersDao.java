package org.chats.dao;

import static org.springframework.data.mongodb.core.query.Criteria.where;

import java.util.List;

import org.chats.db.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

public class UsersDao {
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	public UserDto getUserById(String userId){
		return mongoTemplate.findById(userId, UserDto.class);
	}
	
	public void addUser(UserDto user){
		mongoTemplate.save(user);
	}
	
	public boolean isUserExists(String userPhone){
		return mongoTemplate.exists(new Query(new Criteria().orOperator(where("phone").regex(userPhone + "$"), where("altPhone").regex(userPhone + "$"))), UserDto.class);
	}
	
	public UserDto getUserByPhoneAndCode(String phone, String code){
		Query query = new Query();
		query.addCriteria(where("phone").is(phone).and("code").is(code));
		return mongoTemplate.findOne(query, UserDto.class);
	}
	
	public List<UserDto> getRegisteredUsers(List<String> phones){
		Query query = new Query();
		query.addCriteria(where("phone").in(phones));
		return mongoTemplate.find(query, UserDto.class);
	}
	
	public UserDto getUserByPhone(String phone){
		Query query = new Query();
		query.addCriteria(where("phone").is(phone));
		return mongoTemplate.findOne(query, UserDto.class);
	}

	public void updateUser(UserDto updatedUser){
		mongoTemplate.save(updatedUser);
	}
	
	public List<UserDto> getAllUsers(){
		return mongoTemplate.findAll(UserDto.class);
	}
	
	public void removeUser(String userId){
		UserDto user = mongoTemplate.findById(userId, UserDto.class);
		mongoTemplate.remove(user);
	}
}
