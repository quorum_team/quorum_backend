package org.chats.dao;

import static org.springframework.data.mongodb.core.query.Criteria.where;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.chats.authorization.ContextHolder;
import org.chats.db.dto.GroupDocumentDto;
import org.chats.db.dto.TaskDto;
import org.chats.enums.Roles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

public class GroupDocumentsDao {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Autowired
	private ContextHolder contextHolder;
	
	public static Map<Roles, String> roles = new HashMap<Roles, String>();
	
	static {
		roles.put(Roles.ADMIN, "admins");
		roles.put(Roles.AUTHOR, "authors");
		roles.put(Roles.USER, "users");
	}
	
	public void create(GroupDocumentDto groupDocument){
		groupDocument.setCreatedAt(System.currentTimeMillis());
		groupDocument.setCreatedBy(contextHolder.getLoggedUserPhone());
		mongoTemplate.save(groupDocument);
	}
	
	public void update(GroupDocumentDto groupDocument, String updaterPhone){
		groupDocument.setUpdatedAt(System.currentTimeMillis());
		groupDocument.setUpdatedBy(updaterPhone);
		update(groupDocument);
	}
	
	public void update(GroupDocumentDto groupDocument){
		mongoTemplate.save(groupDocument);
	}
	
	public void delete(GroupDocumentDto groupDocument){
		mongoTemplate.remove(groupDocument);
	}
	
	public GroupDocumentDto getById(String id){
		return mongoTemplate.findById(id, GroupDocumentDto.class);
	}
	
	public List<GroupDocumentDto> getAll(String userPhone){
		List<GroupDocumentDto> result = new ArrayList<GroupDocumentDto>();
		Query query = new Query();
		Criteria criteria = new Criteria();
		query.addCriteria(criteria.orOperator(where("roles.authors").in(new Object[] {userPhone}),where("roles.admins").in(new Object[] {userPhone}), where("roles.users").in(new Object[] {userPhone})));
		result.addAll(mongoTemplate.find(query, GroupDocumentDto.class));
		return result;
	}
	
	public void pushTask(TaskDto task, String groupId){
		mongoTemplate.updateFirst(new Query(where("id").is(groupId)),
				new Update().push("tasks", task),
				GroupDocumentDto.class);
	}
	
	public void pullTask(String taskId){
		mongoTemplate.updateFirst(new Query(where("tasks").elemMatch(where("id").is(taskId))),
				new Update().pull("tasks",  new Query(where("id").is(taskId))),
				GroupDocumentDto.class);
	}
	
	public void removeUserRole(String groupId, String phone){
		mongoTemplate.updateFirst(new Query(where("id").is(groupId)),
				new Update().pull("roles.admins", phone),
				GroupDocumentDto.class);
		mongoTemplate.updateFirst(new Query(where("id").is(groupId)),
				new Update().pull("roles.authors", phone),
				GroupDocumentDto.class);
		mongoTemplate.updateFirst(new Query(where("id").is(groupId)),
				new Update().pull("roles.users", phone),
				GroupDocumentDto.class);
	}
	
	public GroupDocumentDto assignUserRole(Roles type, String userPhone, String groupId){
		mongoTemplate.updateFirst(new Query(where("id").is(groupId)),
				new Update().push(String.format("roles.%s", roles.get(type)), userPhone),
				GroupDocumentDto.class);
		return mongoTemplate.findById(groupId, GroupDocumentDto.class);
	}
}
