package org.chats.message;

import org.chats.dao.MessagesDao;
import org.chats.dao.RegistrationDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

public class MessagesService {
	
	@Autowired
	private MessagesDao messagesDao;
	
	@Autowired
	private RegistrationDao registrationDao;
	
	/*
	 * Fired every 6 hours
	 */
	@Scheduled(cron="0 0 0/6 1/1 * ?")
	public void cleanUp(){
		messagesDao.removeMonthOldMessages();
	}

	@Scheduled(cron="0 0 0 1/2 * ?")
	public void cleanUpRegistrations(){
		registrationDao.cleanDates();
	}
}
