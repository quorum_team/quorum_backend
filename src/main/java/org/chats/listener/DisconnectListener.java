package org.chats.listener;

import org.chats.authorization.ContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

public class DisconnectListener implements ApplicationListener<SessionDisconnectEvent> {
	
	@Autowired
	ContextHolder contextHolder;

	@Override
	public void onApplicationEvent(SessionDisconnectEvent wsSession) {
		contextHolder.removeSessionPhone(wsSession);
	}
}
