package org.chats.listener;

import org.chats.authorization.ContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.web.socket.messaging.SessionConnectEvent;

public class ConnectListener implements ApplicationListener<SessionConnectEvent> {
	
	@Autowired
	ContextHolder contextHolder;
	
	@Override
	public void onApplicationEvent(SessionConnectEvent wsSession) {
		contextHolder.addSessionPhone(wsSession);
	}
}
