package org.chats.event;

import static org.chats.event.WebSocketMessage.send;

public class MessageDecorator {
	
	private String entity;
	
	protected void sendUpsert(String phone, Object message, String... subentity){
		send(getEntity(subentity), "/upsert", phone, message);
	}
	
	protected void sendDelete(String phone, Object message, String... subentity){
		send(getEntity(subentity), "/delete", phone, message);
	}
	
	protected void sendDraft(String phone, Object message, String... subentity){
		send(getEntity(subentity), "", phone, message);
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}
	
	private String getEntity(String[] args){
		return getEntity() + (args.length > 0 ? "_" + args[0] : "");
	}

}
