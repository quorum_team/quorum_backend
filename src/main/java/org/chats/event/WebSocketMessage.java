package org.chats.event;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageSendingOperations;

public class WebSocketMessage {
	
	private static SimpMessageSendingOperations messagingTemplateStatic;
	private final static String MESSAGE_DESTINATION = "/%s%s/%s";

	@PostConstruct
	public void init() {
		messagingTemplateStatic = messagingTemplate;
	}

	@Autowired
	private SimpMessageSendingOperations messagingTemplate;

	public static void send(String entity, String type, String phone, Object message) {
		messagingTemplateStatic.convertAndSend(String.format(MESSAGE_DESTINATION, entity, type, phone), message);
	}
}
