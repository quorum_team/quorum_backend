package org.chats.db.dto;


public class ResultDto extends AbstractDto {
	
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
