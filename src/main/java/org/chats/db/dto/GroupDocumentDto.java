package org.chats.db.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "groupDocuments")
@XmlRootElement (name = "groupDocuments")
public class GroupDocumentDto {

	@Id
	@XmlElement
	private String id = UUID.randomUUID().toString();
	
	@XmlElement
	private List<TaskDto> tasks = new ArrayList<TaskDto>();
	
	@XmlElement
	private String createdBy;
	
	@XmlElement
	private String icon;
	
	@XmlElement
	private String updatedBy;
	
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(long createdAt) {
		this.createdAt = createdAt;
	}

	public long getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(long updatedAt) {
		this.updatedAt = updatedAt;
	}

	@XmlElement
	private long createdAt;
	
	@XmlElement
	private long updatedAt;
	
	@XmlElement
	private String title;
	
	@XmlElement
	private RoleDto roles = new RoleDto();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<TaskDto> getTasks() {
		return tasks;
	}

	public void setTasks(List<TaskDto> tasks) {
		this.tasks = tasks;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public RoleDto getRoles() {
		return roles;
	}

	public void setRoles(RoleDto roles) {
		this.roles = roles;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
}
