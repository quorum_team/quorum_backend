package org.chats.db.dto;

import java.util.HashSet;
import java.util.Set;

import org.springframework.data.annotation.Id;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "registration")
@XmlRootElement (name = "registration")
public class RegisterDto {
	
	@XmlElement
	private Set<String> ips = new HashSet<String>();
	
	@Id
	@XmlElement
	private String targetPhone;
	
	@XmlElement
	private Set<Long> dates = new HashSet<Long>();

	public String getTargetPhone() {
		return targetPhone;
	}

	public void setTargetPhone(String targetPhone) {
		this.targetPhone = targetPhone;
	}

	public Set<Long> getDates() {
		return dates;
	}

	public void setDates(Set<Long> dates) {
		this.dates = dates;
	}

	public Set<String> getIps() {
		return ips;
	}

	public void setIps(Set<String> ips) {
		this.ips = ips;
	} 

}
