package org.chats.db.dto;

import java.util.UUID;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Document(collection = "abstract")
@XmlRootElement (name = "abstract")
public class AbstractDto {

	@Id
	@XmlElement
	@JsonInclude(Include.NON_NULL)
	private String id;
	
	public AbstractDto(){
		if (StringUtils.isEmpty(getId())){
			setId(UUID.randomUUID().toString());
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
