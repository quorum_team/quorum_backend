package org.chats.db.dto;

import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "roles")
@XmlRootElement (name = "roles")
public class RoleDto {

	@XmlElement
	private Set<String> admins = new HashSet<String>();
	
	@XmlElement
	private Set<String> authors = new HashSet<String>();
	
	@XmlElement
	private Set<String> users = new HashSet<String>();

	public Set<String> getAdmins() {
		return admins;
	}

	public void setAdmins(Set<String> admins) {
		this.admins = admins;
	}

	public Set<String> getAuthors() {
		return authors;
	}

	public void setAuthors(Set<String> authors) {
		this.authors = authors;
	}

	public Set<String> getUsers() {
		return users;
	}

	public void setUsers(Set<String> users) {
		this.users = users;
	}
}
