package org.chats.db.dto;

import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.chats.enums.MessageType;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "messagesContent")
@XmlRootElement (name = "messagesContent")
public class MessageContentDto extends AbstractDto implements Comparable<MessageContentDto>{

	@XmlElement
	private String content;
	
	@XmlElement
	private String groupId;
	
	@XmlElement
	private String createBy;
	
	@XmlElement
	private long createAt;
	
	@XmlElement
	private MessageType type;
	
	@XmlElement
	private Set<String> users = new HashSet<String>();
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	public String getGroupId() {
		return groupId;
	}
	
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	
	public String getCreateBy() {
		return createBy;
	}
	
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	
	public long getCreateAt() {
		return createAt;
	}
	
	public void setCreateAt(long createAt) {
		this.createAt = createAt;
	}

	public MessageType getType() {
		return type;
	}

	public void setType(MessageType type) {
		this.type = type;
	}

	public Set<String> getUsers() {
		return users;
	}

	public void setUsers(Set<String> users) {
		this.users = users;
	}

	@Override
	public int compareTo(MessageContentDto o) {
		return createAt > o.createAt ? 1 : createAt == o.createAt ? 0 : -1;
	}
}
