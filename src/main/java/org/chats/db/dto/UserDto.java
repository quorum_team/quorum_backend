package org.chats.db.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.chats.enums.UserType;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.mobile.device.DevicePlatform;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Document(collection = "users")
@XmlRootElement (name = "users")
@JsonInclude(Include.NON_NULL)
public class UserDto extends AbstractDto{
	
	@XmlElement
	private String name;
	
	@XmlElement
	private String countryExt;
	
	@XmlElement
	private DevicePlatform device;
	
	@XmlElement
	private String phone;
	
	@XmlElement
	private String altPhone;

	@XmlElement
	private String code;
	
	@XmlElement
	private UserType userType; 
	
	public UserDto(){
		super();
		UserType userType = getUserType();
		if (userType== null){
			setUserType(UserType.REGULAR);
		}
	}
	
	public String getCountryExt() {
		return countryExt;
	}

	public void setCountryExt(String countryExt) {
		this.countryExt = countryExt;
	}
	
	public DevicePlatform getDevice() {
		return device;
	}

	public void setDevice(DevicePlatform device) {
		this.device = device;
	}
	
	public String getAltPhone() {
		return altPhone;
	}

	public void setAltPhone(String altPhone) {
		this.altPhone = altPhone;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public UserType getUserType() {
		return userType;
	}
	public void setUserType(UserType userType) {
		this.userType = userType;
	}
}
